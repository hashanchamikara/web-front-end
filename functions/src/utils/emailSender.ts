import * as functions from 'firebase-functions';
import * as nodemailer from 'nodemailer'

/**
 * Send email
 *
 * @param {string} type - Email type for logging purpose
 * @param {string} recipientEmail - multiple address can be given seperated by comma (,)
 * @param {string} subject
 * @param {string} htmlContent
 * @returns {Promise}
 */
const sendEmail = function (recipientEmail: string, subject: string, htmlContent: string): Promise<any> {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.LOGIN_USERNAME,
      pass: process.env.LOGIN_PASSWORD,
    },
  });
  return transporter.sendMail({
    from: process.env.SENDER_EMAIL_ADDRESS,
    to: recipientEmail,
    subject: subject,
    html: htmlContent
  }).then(function (info) {
    console.log(info);
  }).catch(function (err) {
    console.log(err);
  });
};


/**
 * Send welcome email upon user creation
 *
 * @type {CloudFunction<Change<DocumentSnapshot>>}
 */
export const emailNewUser = functions.firestore.document('/users/{doc_id}').onCreate(
  (snap, context) => {
    const data = snap.data();
    return sendEmail(data.email, `Welcome ${data.displayName} to Satipasala!`, "The Sati Pasala Foundation aims at sharing mindfulness with students, teachers, and school and university communities, as well as those in other relevant sectors. The Foundation a non-profit Organisation, which is non-income generating, and is entirely managed through donor funding. It is free from ethnic, religious & political dimensions and promotes unity and harmony amongst all communities and people.\n" +
      "\n" +
      "The Sati Pasala Foundation is managed by a team of volunteers who are mindfulness practitioners, dedicated to sharing the understanding and practice of mindfulness, across all borders and with all walks of life. Sati Pasala is based on the understanding that the power and potency of mindfulness can heal and unite, transcending all boundaries and barriers.")
      .then(() => console.log("Welcome email message is sent to %s", data.email))
      .catch(() => console.error("Welcome email message couldn't be sent to %s", data.email));
  }
);

/**
 * Send email upon host creation
 *
 * @type {CloudFunction<Change<DocumentSnapshot>>}
 */
export const emailNewHost = functions.firestore.document('/hosts/{doc_id}').onCreate(
  (snap, context) => {
    const data = snap.data();
    return sendEmail(data.email, `New host - ${data.name} added to Satipasala!`, "New host added")
      .then(() => console.log("New host email message is sent to %s", data.email))
      .catch(() => console.error("New host email message couldn't be sent to %s", data.email));
  }
);

