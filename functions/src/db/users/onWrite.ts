import * as functions from "firebase-functions";
import * as admin from "firebase-admin";


// This function will update the customClaims in the user token, if the users Role changes.
export const dbUsersOnWrite = functions.firestore.document('users/{userId}').onWrite((snap, context) => {

  // Exit when the data is deleted.
  if (!snap.after.exists) {
    return null;
  }

  const userData = snap.after.data();
  const oldUserData = snap.before.data();

  console.log('userData');
  console.log(userData);

  if (oldUserData && userData && userData.uid) {
    const userId: string = userData.uid;

    // Update claims only if the role has changed.
    if (userData.userRole !== oldUserData.userRole) {

      // Update claims only if permissions are set.
      if (userData.userPermissions) {
        const customClaims = {
          userPermissions: userData.userPermissions
        };

        return admin.auth().setCustomUserClaims(userId, customClaims)
          .then(() => {
            // Tell client to refresh token on user.
            console.log('claims updated successfully');
          })
          .catch(error => {
            console.log(error);
          })
      }
    }
  }

  return null;
  //return snap.after.ref.set({userPriviledges:"TEST"},{merge: true});
});
