import {Label} from "./Label";
import {QuestionType} from "./referencedata/QuestionType";
import {Chip, FormField} from "@satipasala/core";

export interface ScoringMechanism {
  id: string;
  name: string;
  description: string;
  type: "SCALE" | "REVERSE_SCALE"
}


export interface Question<VALUE_TYPE> extends FormField<VALUE_TYPE>{
  id:string;
  description?:string
  questionType:QuestionType;
  labels:Label[];
  scoringMechanism:ScoringMechanism
  questionLevel:QuestionLevel;
}

export interface QuestionLevel extends Chip{
}
