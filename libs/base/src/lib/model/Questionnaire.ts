
import {Question} from "./Question";

export interface Questionnaire {
  /*type:QuestionnaireType;*/
  name: string;
  id:string;
  questions: Question<any>[];
  occurence?:number;
}


export enum QuestionnaireType {
  AGGRIGATED,
  INDIVIDUAL

}
