
import {UserInfo} from "firebase";
import {HostInfo} from "./Host";
import {CountryInfo} from "./country/Country";
import {ProvinceInfo} from "./country/Province";
import {CityInfo} from "./country/City";
import {StateInfo} from "./country/State";
import {LocationInfo} from "./HostLocation";
import {UserInfo as FirebaseUserInfo} from "firebase";
import {Role} from "./Role";
export interface User extends UserInfo{

  password?: string | null; //todo remove this field

  street?: string | null;
  city?: CityInfo | null;
  stateInfo?: StateInfo | null;
  province?: ProvinceInfo | null;
  country?: CountryInfo | null;
  organizations?: HostInfo[] | null;
  locations?:LocationInfo[]
  preferredMedium?: string | null;

  status?: string | null;
  createdAt?: string | null;
  updatedAt?: string | null;
  courseSubscriptions?:Object; //object key is CourseSubscription.userInfo.uid+"_"+CourseSubscription.course.id

}


export interface UserInfo extends FirebaseUserInfo{ //FirebaseUsr represents user account
  id:string;
  userName?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  dob?: string | null;
  nic?: string | null;
  userRole?: Role;
  userPermissions?:string[]
}

