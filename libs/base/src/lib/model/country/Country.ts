import {Province} from "./Province";
import {LandType} from "./Types";
import {State} from "./State";
import {Language} from "./Language";

export interface Country extends CountryInfo {
  population?: number;
  locationCount?: number;
  states: State[];
  languages: Language[];
  provinces?: Province[]; //not present in the country databases.so set it to be optional.
  native: string;
  emojiU: string;
  phone: string;
  continent: string;
  capital: string;
  currency: string;
  sortname: string
}

export interface CountryInfo {
  id: string;
  name: string;
  emoji: string;
  shortName: string;
  type: LandType;

}
