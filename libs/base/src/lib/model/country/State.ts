import {LandType} from "./Types";
import {City} from "./City";

export interface State extends StateInfo{
  locationCount?:number;
  cities:City[];
  countryEmojiU:string;
}

export interface StateInfo {
  id:string;
  name:string;
  country_id:string;
  countryShortName:string;
  countryEmoji:string;
  countryName:string;
  type?:LandType;
}
