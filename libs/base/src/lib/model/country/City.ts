import {LandType} from "./Types";


export interface CityInfo {
  id:string;
  name:string;
  type?:LandType;
  state_id:string
  stateName:string
  countryShortName:string;
  countryName:string;
  countryEmoji:string;
}

export interface City extends CityInfo{
  countryEmojiU:string;
  locationCount?:number;
}
