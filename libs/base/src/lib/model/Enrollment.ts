
import {LocationInfo} from "./HostLocation";
import {HostInfo} from "./Host";
import {UserInfo} from "firebase";
import {Course} from "./Course";

export interface Enrollment{
  id:string
  userInfo: UserInfo;
  teacherInfo:UserInfo;
  locationInfo:LocationInfo,
  hostInfo:HostInfo,
  course: Course;
}

