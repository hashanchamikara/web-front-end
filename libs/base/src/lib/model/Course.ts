
import {Questionnaire} from "./Questionnaire";
import {HostLocation} from "./HostLocation";
import {Host} from "./Host";
import {Activity} from "./referencedata/Activity";
export interface Course {
  id :string
  name : string
  description :string
  facilitatorsCount:number;
  numberOfFeedback:number;
  activities:Array<Activity>;
  questionnaire:Questionnaire;
  startDate? :string
  endDate? :string
  createdAt?:Date
  updatedAt?:Date;

  location?:HostLocation;
  organization?:Host;
}
