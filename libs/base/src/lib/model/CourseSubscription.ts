
import {UserInfo} from "./User";
import {Course} from "./Course";

export interface CourseSubscription {
  id?:string | null;
  course:Course;
  userInfo:UserInfo;
  teacherInfo?:UserInfo | null;
  isMandatory?:boolean | null;
}
