import {Activity} from "./Activity";
import {FeedbackForm} from "./FeedbackForm";
import {RefData} from "./RefData";

export interface Program extends RefData {
  id?: string;
  name: string;
  description?: string;
  activities: Activity[];
  facilitatorCount: number;
  feedbackCollectionCount: number;
  feedbackForms: FeedbackForm[];
}
