import {RefData} from "./RefData";

export class QuestionType extends RefData {
  public id: string;
  public type: string;
  public label: string;
  public fieldType: string;
  public answerType: string;
}
