import {ActivityType} from "./ActivityType";
import {RefData} from "./RefData";

export class Activity extends RefData {
  public id: string;// Added ID for backward compatibility. This is not required.
  public name:string;
  public description: string;
  public type: ActivityType;
  public maxPoints: number;
  public gradable: string;
}
