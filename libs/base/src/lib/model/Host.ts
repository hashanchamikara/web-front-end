import {Course} from "./Course";
import {User} from "./User";


//for de normalizing purpose
export interface HostInfo {
  id:string;
  name:string;
  description:string;
}

export interface Host extends HostInfo{
  locations: Location[];
  schoolDetails: any;
  medium:string;
  phone_number:string;
  business_reg_no:string;
  website:string;
  email:string;
  street:string;
  city:string;
  province:string;
  country:string;
  type:string;
  state:string;
  courses?:Course[];
  students?:User[];
  teachers?:User[];
}

export interface Grade {
  grade: string;
  classes: string;
}

export interface Location {
  locationType: string;
  locationSubType: string;
}


