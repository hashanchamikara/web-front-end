import {CourseSubscription, Questionnaire} from "@satipasala/base";
import {Occurrence} from "./Occurrence";

export interface Feedback extends CourseSubscription{
  feedback:Questionnaire;
  occurrence:Occurrence;
}



