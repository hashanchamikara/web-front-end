export interface LocationType {
  id:string;
  description:string;
  name:string;
}
