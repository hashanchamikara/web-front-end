import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, DocumentReference, QueryFn} from "@angular/fire/firestore";
import {HostLocation} from "../model/HostLocation";
import {CollectionService} from "../impl/CollectionService";

export class LocationsService extends CollectionService<HostLocation> {
  public static collection: string = "locations";

  constructor(protected fireStore: AngularFirestore) {
    super(LocationsService.collection, fireStore);
  }
}
