import { Injectable } from '@angular/core';
import {CollectionService} from "@satipasala/base";
import {AngularFirestore} from "@angular/fire/firestore";
import { Feedback } from '../model/Feedback';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService extends CollectionService<Feedback> {
  public static collection: string = "feedback";

  constructor(protected fireStore: AngularFirestore) {
    super(FeedbackService.collection, fireStore);
  }
}
