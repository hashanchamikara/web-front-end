import { TestBed } from '@angular/core/testing';
import {CourseSubscriptionService} from "./course-subscription.service";

describe('UserSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseSubscriptionService = TestBed.get(CourseSubscriptionService);
    expect(service).toBeTruthy();
  });
});
