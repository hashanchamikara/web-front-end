import {AngularFirestore} from "@angular/fire/firestore";
import {CollectionService} from "../impl/CollectionService";
import {User} from "../model/User";

export class UsersService extends CollectionService<User> {
  public static collection: string = "users";

  constructor(protected  fireStore: AngularFirestore) {
    super(UsersService.collection, fireStore);
  }

  /**
   *
   * @param uid ID of the user to find
   * @param userConsumer callback to the value change subscriber
   */
  public getUser(uid, userConsumer) {
    return this.fireStore.collection(this.collection).doc(uid).valueChanges().subscribe(action => userConsumer(action));
  }

  filterData(customfilters) {
    console.log("filtering by quick filters " + customfilters.field+customfilters.criteria+customfilters.filtervalue);
    return new Promise((resolve, reject) => {
      if (customfilters.criteria == '')
        reject();
      if (customfilters.filtervalue == '')
        reject();
      resolve(this.fireStore.collection(this.collection, ref =>
        ref.where(customfilters.field, customfilters.criteria, customfilters.filtervalue)).valueChanges());
    })
  }
}
