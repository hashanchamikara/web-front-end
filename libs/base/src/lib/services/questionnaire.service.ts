import {AngularFirestore} from "@angular/fire/firestore";
import {CollectionService} from "../impl/CollectionService";
import {Questionnaire} from "../model/Questionnaire";

export class QuestionnaireService extends CollectionService<Questionnaire> {

  public static collection: string = "questionnaires";

  constructor(protected fireStore: AngularFirestore) {
    super(QuestionnaireService.collection, fireStore);
  }
}
