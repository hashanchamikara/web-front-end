import {AngularFirestore} from "@angular/fire/firestore";
import {CourseSubscription} from "../model/CourseSubscription";
import {CollectionService} from "../impl/CollectionService";

export class CourseSubscriptionService extends CollectionService<CourseSubscription> {
  public static collection: string = "enrollments";

  constructor(protected fireStore: AngularFirestore) {
    super(CourseSubscriptionService.collection, fireStore);
  }

}
