import {AngularFirestore} from "@angular/fire/firestore";
import {Role} from "../model/Role";
import {CollectionService} from "../impl/CollectionService";


export class RolesService extends CollectionService<Role> {
  public static collection: string = "roles";

  constructor(protected fireStore: AngularFirestore) {
    super(RolesService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getRole(roleId, roleConsumer) {
    return this.fireStore.collection(this.collection).doc(roleId).valueChanges().subscribe(action => roleConsumer(action));
  }
}
