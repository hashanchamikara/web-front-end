import {AngularFirestore} from "@angular/fire/firestore";
import {Permission} from "../model/Permission";
import {CollectionService} from "../impl/CollectionService";

export class PermissionsService extends CollectionService<Permission> {
  public static collection: string = "permissions";

  constructor(protected fireStore: AngularFirestore) {
    super(PermissionsService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getPermission(permissionId, permissionConsumer) {
    return this.fireStore.collection(this.collection).doc(permissionId).valueChanges().subscribe(action => permissionConsumer(action));
  }
}
