import {NgModule} from '@angular/core';
import {AngularFirestore, AngularFirestoreModule} from "@angular/fire/firestore";
import {AnswerOptionsComponent} from "./questions/components/answer-options/answer-options.component";
import {AnswerWrittenComponent} from "./questions/components/answer-written/answer-written.component";
import {AnswerNumberComponent} from "./questions/components/answer-number/answer-number.component";
import {AnswerSelectComponent} from "./questions/components/answer-select/answer-select.component";
import {AnswerDropDownComponent} from "./questions/components/answer-drop-down/answer-drop-down.component";
import {SQuestionnaireForm} from "./questions/components/questionnaire-form/s-questionnaire-form.component";
import {SQuestionComponent} from "./questions/components/question/s-question.component";
import {CommonModule} from "@angular/common";
import {AngularFireAuth, AngularFireAuthModule} from "@angular/fire/auth";
import {CoreModule} from "@satipasala/core";
import {HostsService} from "./services/hosts.service";
import {LocationsService} from "./services/locations.service";
import {AuthService} from "./services/auth.service";
import {NotifyService} from "./services/notify.service";
import {CoursesService} from "./services/courses.service";
import {QuestionnaireService} from "./services/questionnaire.service";
import {QuestionsService} from "./services/questions.service";
import {ReferenceDataService} from "./services/reference-data.service";
import {UsersService} from "./services/users.service";
import {PermissionsService} from "./services/permissions.service";
import {CourseSubscriptionService} from "./services/course-subscription.service";
import { SearchBoxComponent } from '../../../core/src/lib/search-box/search-box.component';

@NgModule({

  declarations: [AnswerOptionsComponent, AnswerWrittenComponent, AnswerNumberComponent, AnswerSelectComponent,
    AnswerDropDownComponent, SQuestionnaireForm, SQuestionComponent],
  imports: [
    CommonModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    CoreModule
  ],
  exports: [SQuestionnaireForm],

  providers: [
    AngularFireAuth,
    AngularFirestore,
    HostsService,
    LocationsService,
    AuthService,
    NotifyService,
    CoursesService,
    QuestionnaireService,
    QuestionsService,
    ReferenceDataService,
    PermissionsService,
    UsersService,
    NotifyService,
    CourseSubscriptionService
  ]
})
export class BaseModule {
}
