import {DataSource} from "@angular/cdk/table";
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {Observable, Subject} from 'rxjs';
import {AngularFirestoreCollection, Query, QueryFn, FieldPath} from "@angular/fire/firestore";
import {AfterViewInit} from "@angular/core";
import {CollectionService, SubCollectionInfo} from "./CollectionService";

/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class FirebaseDataSource<T> extends DataSource<T> implements AfterViewInit {
  data: AngularFirestoreCollection<T>;
  total: number = 100;//todo add total calculation to each collection in functions
  lastObj: T;
  dataSubject: Subject<T[]> = new Subject<T[]>();
  collectionService: CollectionService<any>;
  //collection filters
  filters: Filter[] = [];
  filterChangeSubject: Subject<boolean> = new Subject<boolean>();
  orderBy: OrderBy[] = [{fieldPath: "id"}];

  public constructor(protected matPaginator: MatPaginator, protected matSort: MatSort, protected colService: CollectionService<any>) {
    super();
    this.collectionService = colService;
    this.filterChangeSubject.subscribe(value => {
      this.fetchData();
    })
  }

  /**
   * query data of a sub collection by giving documentId and sub collection name
   * @param pageIndex which index of page is retrieved
   * @param pageSize
   * @param subCollectionPaths [{documentId: 'course1', subCollection:'activities'}]
   */
  public queryData(queryFn: QueryFn, ...subCollectionPaths: SubCollectionInfo[]) {
    this.collectionService.querySubCollection(
      queryFn,
      ...subCollectionPaths
    ).subscribe(
      document => {
        this.dataSubject.next(document);
      });
  }

  /**
   * query data from multiple sub collections by giving documentId and sub collection name as rest parameters
   *
   * @param pageIndex which index of page is retrieved
   * @param pageSize
   * @param subCollectionPaths [{documentId: 'course1', subCollection:'activities'}]
   */
  public queryCombinedData(queryFn: QueryFn, subCollectionPaths: SubCollectionInfo[][]) {
    for (let i = 0; i < subCollectionPaths.length; i++) {
      this.queryData(queryFn, subCollectionPaths[i][0]);
    }
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<T[]> {
    return this.dataSubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.dataSubject.unsubscribe();
  }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: AngularFirestoreCollection<T>) {/*
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);*/
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: AngularFirestoreCollection<T>) {
    /*if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });*/
  }

  ngAfterViewInit(): void {
    this.fetchData();
  }

  /**
   * load more data to collection using paginator
   * @param event
   */
  loadMore(event: PageEvent) {
    this.queryData(query => this.addWhereClauses(this.addOrderByClauses(query)).startAt(event.pageIndex).limit(event.pageSize));
  }

  fetchData(): void {
    this.queryData(query => this.addWhereClauses(this.addOrderByClauses(query)).startAt(0).limit(this.matPaginator.pageSize));
  }

  setOrderBy(...orderBy: OrderBy[]) {
    this.orderBy = orderBy;
  }

  addOrderBy(orderBy: OrderBy){
    this.orderBy.push(orderBy);
  }

  private addOrderByClauses(query: Query) {
    this.orderBy.forEach(order => {
      query = this.addOrderByClause(query, order)
    });
    return query;
  }

  private addOrderByClause(query: Query, orderBy: OrderBy) {
    return query.orderBy(orderBy.fieldPath, orderBy.directionStr);
  }

  private addWhereClauses(query: Query): Query {
    this.filters.forEach(filter => {
      query = this.addWhereClause(query, filter)
    });
    return query;
  }

  addWhereClause(query: Query, filter: Filter) {
    return query.where(filter.fieldPath, filter.opStr, filter.value)
  }

  addFilter(filter: Filter) {
    this.removeFilter(filter, false);
    this.filters.push(filter);
    this.filterChangeSubject.next(true);
  }

  removeFilter(filter: Filter, fireChanges: boolean = true) {
    let index = this.filters.findIndex(value => (value.fieldPath == filter.fieldPath && value.opStr == filter.opStr));

    if (index != -1) {
      this.filters.splice(index, 1);
    }
    if (fireChanges === true) {
      this.filterChangeSubject.next(false);
    }
  }

  clearFilters() {
    this.filters = [];
  }
}


export type WhereFilterOp = '<' | '<=' | '==' | '>=' | '>' | 'array-contains';
export type OrderByDirection = 'desc' | 'asc';

export class Filter {
  fieldPath: string;
  opStr: WhereFilterOp;
  value: any;
}

export class OrderBy {
  fieldPath: string;
  directionStr?: OrderByDirection
}
