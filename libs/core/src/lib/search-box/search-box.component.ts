import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { SearchService } from '../../../../base/src/lib/services/search.service';
import {Observable, Subscription} from 'rxjs';
import { SearchFilter } from '../../../../base/src/lib/model/SearchFilter';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'satipasala-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit, OnChanges {
  @Input() displayField: string;
  @Input() collection: string;
  @Input() searchFields: string[];

  searchFormControl: FormControl = new FormControl();
  searchObservable: Observable<any>;
  optionsArray: any[] = [];
  searchString: string = '';
  displayString: string;
  subscription:Subscription;

  constructor(private searchService: SearchService) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    if(!this.displayString){
      this.displayString =this.searchString;
    }
  }

  search($event: string) {
    this.searchString = $event;
    if(this.subscription){
      this.subscription.unsubscribe();
      this.subscription = null;
    }
    this._filter(this.searchString);
  }

  private _filter(value: string) {
    this.optionsArray = [];
    if(this.searchString.length <= 2){
      return;
    }
    let searchFilters: SearchFilter[] = [];
    this.searchFields.forEach(searchField => {
      searchFilters.push({ field: searchField, value: this.searchString });
    });
    if (this.searchFields.length > 0) {
      this.searchObservable = this.searchService.searchCollection(this.collection, ...searchFilters);

      this.subscription = this.searchObservable.subscribe(next => {
        // console.log(next);
        this.optionsArray.push(next);
      });
    }
  }

  displayFunction(val?: any): string | undefined {
    console.log(val);
    console.log(this.displayString);
    return val ? val[this.displayString] : undefined;
  }
}
