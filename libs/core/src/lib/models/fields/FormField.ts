import {Validator} from "./Validator";
import {FireArrayField} from "./FireArrayField"
import {ChipList} from "../ChipList";
import {Option} from "./Option";

export interface FormField<T> {
  order: number;
  type: string;
  name: string;
  label: string;
  disabled:boolean;
  value: T,
  hint?: string;
  validators?:FireArrayField<Validator>;
  formChipList?:ChipList;
  icon?:string;
  options?:Array<Option<T>>;
  checked?:boolean;
  indeterminate?:boolean;
  labelPosition?:string;
}
