export class ObjectUtils {

  static reduceProperties(source, destination) {
    for (var prop in destination) {
      if (source[prop] && destination.hasOwnProperty(prop)) {
        destination[prop] = source[prop];
      }
    }

    return destination;
  }
}
