import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SDragDropList,} from './drag-drop/s-drag-drop-list/s-drag-drop-list.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {A11yModule} from "@angular/cdk/a11y";
import {CdkStepperModule} from "@angular/cdk/stepper";
import {CdkTreeModule} from "@angular/cdk/tree";
import {CdkTableModule} from "@angular/cdk/table";
import {SGrid} from './grid/s-grid-component/s-grid.component';
import {MaterialModule} from "../../../../apps/admin/src/app/imports/material.module";
import {SChip} from './chip/s-chip/s-chip.component';
import {SDragDropListItem} from "./drag-drop/s-drag-drop-list-item/s-drag-drop-list-item.component";
import {SChipList} from "./chip/s-chip-list/s-chip-list.component";
import {SGridTile} from './grid/s-grid-tile/s-grid-tile.component';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatAutocompleteModule
} from "@angular/material";
import {CarouselSliderComponent} from "./carousel-slider/carousel-slider.component";
import {CarouselModule} from "ngx-owl-carousel-o";
import {DynamicFormComponent} from "./dynamic-forms/dynamic-form-component/dynamic-form.component";
import {CheckBoxComponent} from "./dynamic-forms/fields/check-box/check-box.component";
import {DropDownComponent} from "./dynamic-forms/fields/drop-down/drop-down.component";
import {RadioComponent} from "./dynamic-forms/fields/radio/radio.component";
import {TextBoxComponent} from "./dynamic-forms/fields/text-box/text-box.component";
import {FieldBuilderComponent} from "./dynamic-forms/field-builder/field-builder.component";
import {InputComponent} from "./dynamic-forms/fields/input/input.component";
import {SFieldError} from "./dynamic-forms/field-error/s-field-error.component";
import {
  FormFieldCustomControlExample,
  MyTelInput
} from "./dynamic-forms/fields/example-tel-input/example-tel-input.component";
import {SSelectionListComponent} from "./dynamic-forms/fields/s-selection-list/s-selection-list.component";
import {DynamicStepperForm} from "./dynamic-forms/dynamic-stepper-form-component/dynamic-stepper-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ErrorStateMatcherFactory} from "./dynamic-forms/services/ErrorStateMatcherFactory";
import { SQuestionComponent } from '../../../base/src/lib/questions/components/question/s-question.component';
import { SearchBoxComponent } from './search-box/search-box.component';

@NgModule({
  declarations: [
    SDragDropList, SDragDropListItem, SGrid, SChip, SChipList, SGridTile, CarouselSliderComponent,
    DynamicFormComponent,
    CheckBoxComponent, DropDownComponent, RadioComponent, TextBoxComponent, FieldBuilderComponent, InputComponent,
    SFieldError, MyTelInput, FormFieldCustomControlExample, SSelectionListComponent, DynamicStepperForm, SearchBoxComponent],
  imports: [
    CommonModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MaterialModule,
    MatInputModule,
    CarouselModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatCheckboxModule,
    MatAutocompleteModule
  ],
  exports: [
    SDragDropList, SDragDropListItem, SGrid, SChip, SChipList, SGridTile, CarouselSliderComponent,
    DynamicFormComponent, DynamicStepperForm, SearchBoxComponent]
  ,

  providers: [ErrorStateMatcherFactory],
})
export class CoreModule {
}
