import {ChangeDetectorRef, Component, Input, OnInit, Optional, Self} from '@angular/core';
import {MatFormFieldControl} from "@angular/material";
import {AbstractFieldComponent} from "../AbstractFieldComponent";
import {FormField} from "../../../models/fields/FormField";
import {ErrorStateMatcherFactory} from "../../services/ErrorStateMatcherFactory";

@Component({
  selector: 'satipasala-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss'],
  providers: [{provide: MatFormFieldControl, useExisting: TextBoxComponent}],
})
export class TextBoxComponent extends AbstractFieldComponent<FormField<any>>{
  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    super(errorStateMatcherFactory, cdRef)

  }
}
