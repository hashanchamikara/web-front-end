import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HostManagementRoutingModule } from './host-management-routing.module';
import { HostManagementPageComponent } from './pages/host-management-page/host-management-page.component';
import { HostInfoComponent } from './components/host-info-component/host-info.component';
import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatCardModule,
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HostSubMenuComponent} from "./components/host-sub-menu/host-sub-menu.component";
import {LocationSubMenuComponent} from "./components/location-sub-menu/location-sub-menu.component";
import {LocationInfoComponent} from "./components/location-info-component/location-info.component";
import {BaseModule, HostsService} from "@satipasala/base";
import { LocationFormComponent } from './components/location-form-component/location-form.component';
import { HostFormComponent } from './components/host-form/host-form.component';
import {CoreModule} from "@satipasala/core";
import { UploadsModule } from '../uploads/uploads.module';

@NgModule({
  declarations: [HostManagementPageComponent, HostInfoComponent,HostSubMenuComponent,LocationInfoComponent,LocationSubMenuComponent, LocationFormComponent, HostFormComponent ],
  imports: [
    CommonModule,
    HostManagementRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    ReactiveFormsModule,
    CoreModule,
    BaseModule,
    UploadsModule,
    FormsModule
  ],

  providers: [HostsService]
})
export class HostManagementModule { }
