import {Component, OnInit} from '@angular/core';

import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import * as APP_ROUTES from "../../../../app-routs";
import {Host, HostLocation, HostsService, ReferenceDataService} from "@satipasala/base";
import {stringify} from "querystring";
import {Grade} from "../../../../../../../../libs/base/src/lib/model/Host";

@Component({
  selector: 'admin-host-form',
  templateUrl: './host-form.component.html',
  styleUrls: ['./host-form.component.scss']
})

export class HostFormComponent implements OnInit {

  hostForm: FormGroup;
  public host: Host;
  public hostId: string;

  public formTitle: string;
  public hostTypeChangeHidden: boolean;
  public submitBtnText: string;
  public hostTypes: string[];
  public mediums: string[];
  public schoolTypes: string[];
  public selectedHost: string;
  public selectedSchoolType: string;
  public gradeList: Grade[];
  public selectedMedium: string;

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private HostsService: HostsService, private ReferenceDataService: ReferenceDataService) {
  }

  ngOnInit() {
    this.populateSelections();

    this.hostForm = this.formBuilder.group({
      name: ['', Validators.required],
      preferred_medium: ['', Validators.required],
      description: '',
      phone_number: ['', Validators.required],
      business_reg_no: '',
      website: '',
      email: ['', Validators.email],
      street: ['', Validators.required],
      city: ['', Validators.required],
      province: ['', Validators.required],
      country: ['', Validators.required],
      type: ['', Validators.required],
      schoolType: '',
      pic_name: ['', Validators.required],
      pic_designation: ['', Validators.required],
      pic_phone: ['', Validators.required],
      pic_email: ['', Validators.required],
      coord_1_name: ['', Validators.required],
      coord_1_designation: ['', Validators.required],
      coord_1_phone: ['', Validators.required],
      coord_1_email: ['', Validators.email],
      coord_2_name: '',
      coord_2_designation: '',
      coord_2_phone: '',
      coord_2_email: ['', Validators.email],
      locations: this.formBuilder.array([this.initLocations(),])
    });

    this.route.params.subscribe(params => {
      this.hostId = params.hostId;
      if (params.hostId === 'new') {
        this.formTitle = "Create Host";
        this.submitBtnText = "Add";
        this.hostTypeChangeHidden = false;
        this.host = <Host>{};
      } else {
        this.formTitle = "Edit Host";
        this.submitBtnText = "Update";
        this.hostTypeChangeHidden = true;
        this.HostsService.getHost(params.hostId, this.setHost(this.hostForm));
      }
    });
  }

  setHost(form: FormGroup) {
    return host => {
      if (host) {
        this.selectedHost = host.type;
        this.selectedMedium = host.preffered_medium;
        if (host.type == 'School') {
          this.gradeList = host.location.gradeList;
          this.selectedSchoolType = host.schoolType;
        }
        form.patchValue({
          id: host.id,
          name: host.name,
          description: host.description,
          phone_number: host.phone_number,
          business_reg_no: host.business_reg_no,
          website: host.website,
          email: host.email,
          street: host.street,
          city: host.city,
          province: host.province,
          country: host.country,
          type: host.type,
          preferred_medium: host.preferred_medium,
          pic_name: host.pic_name,
          pic_phone: host.pic_phone,
          pic_designation: host.pic_designation,
          pic_email: host.pic_email,
          coord_1_name: host.coord_1_name,
          coord_1_phone: host.coord_1_phone,
          coord_1_designation: host.coord_1_designation,
          coord_1_email: host.coord_1_email,
          coord_2_name: host.coord_2_name,
          coord_2_phone: host.coord_2_phone,
          coord_2_designation: host.coord_2_designation,
          coord_2_email: host.coord_2_email,
          locations: host.locations
        });
      } else {
        this.backToHostManage();
      }
    };
  }

  initLocations() {
    return this.formBuilder.group({
      locationType: '',
      locationSubType: ''
    })
  }

  addLocations(){
    const locationsControl = <FormArray>this.hostForm.controls['locations'];
    locationsControl.push(this.initLocations());
  }

  removeLocations(i: number){
    const locationsControl = <FormArray>this.hostForm.controls['locations'];
    locationsControl.removeAt(i);
  }

  populateSelections() {
    this.ReferenceDataService.getHostTypes(orgDoc => this.hostTypes = orgDoc.Type);
    this.ReferenceDataService.getMediums(mediumsDoc => this.mediums = mediumsDoc.language);
    this.ReferenceDataService.getSchoolType(schoolDoc => this.schoolTypes = schoolDoc.Type);
  }

  addEditHost() {
    if(!this.hostForm.valid){
      alert("Invalid Form");
      return;
    }
    this.host = this.hostForm.value;
    this.setHostState();
    this.setSchoolDetails();

    if (this.hostId == "new") {
      this.HostsService.add(this.host).then(() => {
        this.backToHostManage()
      }).catch(err => {
        console.error(err)
      })
    } else {
      this.HostsService.update(this.hostId, this.host);
    }
  }

  //TODO: Set host state according to admin type
  private setHostState() {
    this.host.state = "Registered"
  }

  private setSchoolDetails() {
    this.host.schoolDetails = {};
    if (this.host.type == 'School' && this.gradeList != null) {
      this.host.schoolDetails.gradeList = this.gradeList;
    }
  }

  schoolSelected() {
    this.gradeList = null;
    switch (this.selectedSchoolType) {
      case '1-5':
        this.populateClassList(1, 5);
        break;
      case '6-13':
        this.populateClassList(6, 13);
        break;
      case '1-13':
        this.populateClassList(1, 13);
        break;
      default:
        this.gradeList = null;
    }
  }

  orgSelected() {
    this.gradeList = null;
  }

  populateClassList(lower, upper) {
    this.gradeList = [];
    for (let i: number = lower; i <= upper; i++) {
      const gradeVal = i.toLocaleString();
      const classVal = "0";
      const grade = <Grade>{
        grade: gradeVal,
        classes: classVal
      };
      this.gradeList.push(grade);
    }
  }

  backToHostManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
  }
}

