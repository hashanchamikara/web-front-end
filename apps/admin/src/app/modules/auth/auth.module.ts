import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthRoutingModule} from './auth-routing.module';
import {AuthService, BaseModule} from "@satipasala/base";
import {PermissionInfoComponent} from './components/permission-info/permission-info.component';
import {RoleInfoComponent} from './components/role-info/role-info.component';
import {MaterialModule} from "../../imports/material.module";
import {PermissionsService} from "@satipasala/base";
import {RolesService} from "@satipasala/base";
import {RoleSubMenuComponent} from './components/role-sub-menu/role-sub-menu.component';
import {
  MatButtonModule,
  MatCardModule, MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule, MatSelectModule, MatSortModule, MatTableModule
} from "@angular/material";

import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";
import {RoleFormComponent} from './components/role-form/role-form.component';
import {RoleManagementPage} from './pages/role-management/role-management-page.component';
import {PermissionManagementPage} from './pages/permission-management/permission-management-page.component';
import {RolePermissionInfoComponent} from './components/role-permission-info/role-permission-info.component';
import {RolePermissionManagementPage} from './pages/role-permission-management/role-permission-management-page.component';
import {AddRoleFormComponent} from "./components/add-role-form/add-role-form.component";
import {AddRoleManagementComponent} from "./pages/add-role-management/add-role-management.component";
import {CoreModule} from "@satipasala/core";

@NgModule({
  declarations: [PermissionInfoComponent, RoleInfoComponent, RoleSubMenuComponent, RoleFormComponent, RoleManagementPage, PermissionManagementPage, RolePermissionInfoComponent, RolePermissionManagementPage, AddRoleFormComponent, AddRoleManagementComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MaterialModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    ReactiveFormsModule, FormsModule,
    CoreModule,
    BaseModule

  ],
  providers: [AuthService, PermissionsService, RolesService]
})
export class AuthModule {
}
