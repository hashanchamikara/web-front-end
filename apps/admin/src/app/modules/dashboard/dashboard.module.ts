import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { OverviewPageComponent } from './pages/overview/overview-page.component';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import {CoreAdminModule} from "../core/core-admin.module";
import {CoreModule} from "@satipasala/core";
import {BaseModule} from "@satipasala/base";

@NgModule({
  declarations: [OverviewPageComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    CoreAdminModule,
    CoreModule,
    BaseModule
  ]
})
export class DashboardModule { }
