import {Component, Inject, LOCALE_ID, NgZone} from '@angular/core';
import {Router} from "@angular/router";

import {SwUpdate} from "@angular/service-worker";
import {
  AUTH_MANAGEMENT_ROUTE,
  AUTH_MANAGEMENT_ROUTE_PERMISSIONS,
  AUTH_MANAGEMENT_ROUTE_ROLES,
  COURSE_MANAGEMENT_ROUTE,
  COURSE_FEEDBACK_ROUTE,
  DASHBOARD_OVERVIEW_ROUTE,
  DASHBOARD_ROUTE,
  FILE_MANAGEMENT_AUDIO_FILES_ROUTE,
  FILE_MANAGEMENT_IMG_UPLOAD_ROUTE,
  FILE_MANAGEMENT_PDF_UPLOAD_ROUTE,
  FILE_MANAGEMENT_ROUTE,
  HOST_MANAGEMENT_INFO_ROUTE,
  HOST_MANAGEMENT_ROUTE,
  QUESTION_MANAGEMENT_ROUTE,
  QUESTIONNAIRE_MANAGEMENT_ROUTE,
  REFERENCE_DATA_ACTIVITY_ROUTE,
  REFERENCE_DATA_ACTIVITY_TYPE_ROUTE,
  REFERENCE_DATA_QUESTION_TYPE_ROUTE,
  REFERENCE_DATA_ROUTE,
  USERS_MANAGEMENT_ROUTE,
  USERS_REGISTER_ROUTE,
  USERS_ROUTE
} from "../../../app-routs";
import {Location} from "@angular/common";

@Component({
  selector: 'admin-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent {

  title = 'admin';
  //Sidenav responsive
  width;
  height;
  mode: string = 'side';
  open = 'true';
  navList: NavList[];

  constructor(public ngZone: NgZone,
              public route: Router,
              private updates: SwUpdate, @Inject(LOCALE_ID) public locale: string, private location: Location) {

    this.navList = [
      {
        categoryName: 'Dashboard', icon: 'dashboard', dropDown: false, categoryLink: DASHBOARD_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Overview',
              subCategoryLink: DASHBOARD_OVERVIEW_ROUTE,
              subCategoryQuery: {title: 'Overview'},
              visable: true
            }
          ]
      },
      {
        categoryName: 'User Management', icon: 'supervisor_account', dropDown: false, categoryLink: USERS_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Create User',
              subCategoryLink: USERS_REGISTER_ROUTE,
              subCategoryQuery: {title: 'Create User'},
              visable: true
            },
            {
              subCategoryName: 'Manage Users',
              subCategoryLink: USERS_MANAGEMENT_ROUTE,
              subCategoryQuery: {title: 'Manage Users'},
              visable: true
            },
            {
              subCategoryName: 'Text Search',
              subCategoryLink: 'search',
              visable: true
            }
          ]
      }, {
        categoryName: 'Host Management', icon: 'business', dropDown: false, categoryLink: HOST_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Add Host',
              subCategoryLink: 'new',
              subCategoryQuery: {title: 'Overview'},
              visable: true
            },
            {
              subCategoryName: 'Manage Hosts',
              subCategoryLink: HOST_MANAGEMENT_INFO_ROUTE,
              subCategoryQuery: {title: 'Login'},
              visable: true
            }
          ]
      }, {
        categoryName: 'Questionnaire Management',
        icon: 'library_books',
        dropDown: false,
        categoryLink: QUESTIONNAIRE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Manage Questionnaire',
              subCategoryLink: "",
              subCategoryQuery: {title: 'Login'},
              visable: true
            },
            {
              subCategoryName: 'Questions',
              subCategoryLink: QUESTION_MANAGEMENT_ROUTE,
              subCategoryQuery: {title: 'view'},
              visable: true
            }
          ]
      }, {
        categoryName: 'Course Management',
        icon: 'chrome_reader_mode',
        dropDown: false,
        categoryLink: COURSE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Courses',
              subCategoryLink: '',
              visable: true
            },
            {
              subCategoryName: 'Feedbacks',
              subCategoryLink: COURSE_FEEDBACK_ROUTE,
              visable: true
            }
          ]
      },
      {
        categoryName: 'File Management', icon: 'assignment', dropDown: false, categoryLink: FILE_MANAGEMENT_ROUTE,
        subCategory:
          [
            {subCategoryName: 'Assignments', subCategoryLink: '/uploads', visable: true},
            {subCategoryName: 'Image Upload', subCategoryLink: FILE_MANAGEMENT_IMG_UPLOAD_ROUTE, visable: true},
            {subCategoryName: 'PDF Upload', subCategoryLink: FILE_MANAGEMENT_PDF_UPLOAD_ROUTE, visable: true},
            {subCategoryName: 'Audio Files', subCategoryLink: FILE_MANAGEMENT_AUDIO_FILES_ROUTE, visable: true}
          ]
      },
      {
        categoryName: 'Auth Management', icon: 'work', dropDown: false, categoryLink: AUTH_MANAGEMENT_ROUTE,
        subCategory:
          [
            {
              subCategoryName: 'Roles',
              subCategoryLink: AUTH_MANAGEMENT_ROUTE_ROLES,
              subCategoryQuery: {title: 'Roles'},
              visable: true
            },
            {
              subCategoryName: 'Permissions',
              subCategoryLink: AUTH_MANAGEMENT_ROUTE_PERMISSIONS,
              subCategoryQuery: {title: 'Permissions'},
              visable: true
            }
          ]
      },
      {
        categoryName: 'Reference Data', icon: 'perm_data_setting', dropDown: false, categoryLink: REFERENCE_DATA_ROUTE,
        subCategory:
          [
            {subCategoryName: 'Activity Types', subCategoryLink: REFERENCE_DATA_ACTIVITY_TYPE_ROUTE, visable: true},
            {subCategoryName: 'Activities', subCategoryLink: REFERENCE_DATA_ACTIVITY_ROUTE, visable: true},
            {subCategoryName: 'Question Types', subCategoryLink: REFERENCE_DATA_QUESTION_TYPE_ROUTE, visable: true}
          ]
      }
    ];
    this.changeMode();
    window.onresize = (e) => {
      ngZone.run(() => {
        this.changeMode();
      });
    };
  }

  /**
   * reload service worker cached resources when there is an update
   */
  reloeadUpdates() {
    this.updates.available.subscribe(() => {
      this.updates.activateUpdate().then(() => window.location.reload())
    })
  }

  ngOnInit() {
    this.reloeadUpdates();

  }

  changeMode() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    if (this.width <= 800) {
      this.mode = 'over';
      this.open = 'false';
    }
    if (this.width > 800) {
      this.mode = 'side';
      this.open = 'true';
    }
  }

  goBack() {

    this.location.back();
  }
}


export class NavList {
  categoryName: string;
  icon: string;
  dropDown: boolean;
  subCategory: NavListItem[];
  categoryLink: string;

  constructor(_categoryName: string, _icon: string, _dropDown: boolean, categoryLink: string = "", _subCategory: NavListItem[]) {
    this.categoryName = _categoryName;
    this.icon = _icon;
    this.dropDown = _dropDown;
    this.subCategory = _subCategory;
    this.categoryLink = categoryLink;
  }
}

export class NavListItem {
  subCategoryName: string;
  subCategoryLink: string;
  subCategoryQuery?: any;
  visable: boolean;
}
