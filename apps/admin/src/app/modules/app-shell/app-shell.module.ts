import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AppShellRoutingModule} from './app-shell-routing.module';
import {MaterialModule} from "../../imports/material.module";
import {AppShellComponent} from "./app-shell-component/app-shell.component";
import {ShellLoaderComponent} from './components/shell-loader/shell-loader.component';
import {UserLoginPageComponent} from "./pages/login-page/user-login-page.component";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {AngularFireFunctionsModule} from "@angular/fire/functions";
import {BrowserTransferStateModule} from "@angular/platform-browser";
import 'hammerjs';
import {AuthGuard, AuthService, BaseModule} from "@satipasala/base";
import {QuestionnaireModule} from '../questionnaire/questionnaire.module';
import {ErrorStateMatcher, MAT_LABEL_GLOBAL_OPTIONS, ShowOnDirtyErrorStateMatcher} from "@angular/material";
import {AngularFireAuth, AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFirestore, AngularFirestoreModule} from "@angular/fire/firestore";
//import {AngularFontAwesomeModule} from "angular-font-awesome";
import {CoreAdminModule} from "../core/core-admin.module";
import {CoreModule} from "@satipasala/core";

// import * as firebase from "firebase"

@NgModule({
  declarations: [AppShellComponent, ShellLoaderComponent, UserLoginPageComponent],
  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
    // AngularFireStorageModule,
    // AngularFireFunctionsModule,
   // AngularFontAwesomeModule,
    CommonModule,
    CoreAdminModule,
    CoreModule,
    BaseModule,
    BrowserTransferStateModule,
    AppShellRoutingModule,
    QuestionnaireModule,
    // AngularFireAuthModule,
    // AngularFirestoreModule,
    // AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [AngularFireAuth, AuthService, AuthGuard,
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  bootstrap: [AppShellComponent]

})
export class AppShellModule {


  constructor(private angularFirestore: AngularFirestore) {
    this.configureLocalCache();
  }

  private configureLocalCache() {
    //TODO: Enable below to disable firestore cache limit (default: 40MB)
    // this.angularFirestore.firestore.settings({
    //   cacheSizeBytes: firebase.firestore.CACHE_SIZE_UNLIMITED
    // });

    this.angularFirestore.firestore.enablePersistence({synchronizeTabs: true}).then(response => {
      console.info("Offline access enabled!");
    }).catch(err => {
      if (err.code == 'failed-precondition') {
        console.error("Multiple tabs open, persistence can only be enabled in one tab at a a time");
      } else if (err.code == 'unimplemented') {
        console.error("The current browser does not support all of the features required to enable persistence");
      }
    });
  }

}
