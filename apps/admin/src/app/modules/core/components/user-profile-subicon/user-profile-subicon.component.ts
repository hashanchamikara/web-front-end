import {Component, Input} from '@angular/core';
import {AuthService} from "@satipasala/base";


@Component({
  selector: 'user-profile-subicon',
  templateUrl: './user-profile-subicon.component.html',
  styleUrls: ['./user-profile-subicon.component.scss'],
})
export class UserProfileSubIconComponent {

  constructor(public auth: AuthService) { }

  logout() {
    this.auth.signOut();
  }
  @Input('ngClass') public sub_profile;
  public hasDark = false;
  public hasWhite = false;

}

