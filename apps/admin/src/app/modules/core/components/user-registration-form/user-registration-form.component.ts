import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {formatDate} from '@angular/common';
import {AuthService, Role, RolesService, User} from "@satipasala/base";
import {Observable} from "rxjs";
import {FileUploadComponent} from "../../../uploads/file-upload/file-upload.component";

type UserFields = 'firstName' | 'dob' | 'phoneNumber' | 'email' | 'password' | 'city' | 'organization' | 'userRole' | 'preferredMedium';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'admin-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.scss']
})
export class UserRegistrationFormComponent implements OnInit {

  userForm: FormGroup;
  formErrors: FormErrors = {
    'firstName': '',
    'dob': '',
    'phoneNumber': '',
    'email': '',
    'password': '',
    'city': '',
    'organization': '',
    'userRole': '',
    'preferredMedium': ''
};
  validationMessages = {
    'firstName': {
      'required': 'First Name is required.',
      'maxlength': 'First Name cannot be more than 40 characters long.'
    },
    'dob': {
      'required': 'Date of Birth is required.',
    },
    'phoneNumber': {
      'required': 'Phone Number is required.',
    },
    'email': {
      'required': 'User Role is required.',
      'email': 'Email must be a valid email.'
    },
    'password': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'confirmPassword': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'city': {
      'required': 'City is required.'
    },
    'organization': {
      'required': 'Organization is required.'
    },
    'role': {
      'required': 'Role is required.'
    },
    'preferredMedium': {
      'required': 'Preferred Medium is required.'
    }
  };

  userRoles: Observable<Role[]>;

  preferredMediums: string[] = [
    "English",
    "Sinhala",
    "Tamil"
  ];

  imgURL: any;

  constructor(private fb: FormBuilder, private auth: AuthService,  private rolesService: RolesService) {
  }

  @ViewChild(FileUploadComponent, {static: false})
  private uploadComponent: FileUploadComponent;

  ngOnInit() {
    this.userRoles = this.rolesService.getAll();
    console.log("user roles size" + this.userRoles);
    this.imgURL = "https://firebasestorage.googleapis.com/v0/b/sathipasala-41bb8.appspot.com/o/profile_pictures%2Fdefault.png?alt=media&token=3aab0db2-244c-4c3c-8852-9818c8f66604";
    this.buildForm();
  }

  onSubmit() {

    this.uploadComponent.putFiles();

    const userData: User = {

      userName : this.userForm.value['email'],
      uid : this.userForm.value['email'],
      displayName : this.userForm.value['firstName'] + " " + this.userForm.value['lastName'],

      firstName : this.userForm.value['firstName'],
      dob : this.userForm.value['dob'],
      phoneNumber : this.userForm.value['phoneNumber'],
      email : this.userForm.value['email'],
      password : this.userForm.value['password'],

      city : this.userForm.value['city'],
      organizations : [this.userForm.value['organization']],

      userRole : this.userForm.value['userRole'],
      preferredMedium : this.userForm.value['preferredMedium'],

      photoURL : "profile_pictures/" + this.userForm.value['email'],
      providerId : this.userForm.value['firstName'],//TODO: Update with proper provider Id
      lastName : this.userForm.value['lastName'],
      nic : this.userForm.value['nic'],
      street : this.userForm.value['street'],
      province : this.userForm.value['province'],
      country : this.userForm.value['country'],

      userPermissions : ["view_users", "edit_users"], //todo use thusara's module
      status : "Active", //todo reference data - user_status/entity_status
      createdAt : formatDate(new Date(), 'long', 'en'),
      updatedAt:formatDate(new Date(), 'long', 'en')
    } as User;

    this.auth.createFirebaseUser(this.userForm.value['email'], this.userForm.value['password']);
    this.auth.createUserByAdmin(userData).then(() =>
      window.location.reload()
    );
  }

  buildForm() {
    this.userForm = this.fb.group({
      'firstName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'dob': ['', [
        Validators.required
      ]],
      'phoneNumber': ['', [
        Validators.pattern('[0-9]{10}'),
        Validators.required
      ]],
      'userRole': ['', [
        Validators.required,
      ]],
      'email': ['', [
        Validators.required,
        Validators.email,
      ]],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'confirmPassword': ['', [
        Validators.required,
      ]],
      'city': ['', [
        Validators.required,
      ]],
      'organization': ['', [
        Validators.required,
      ]],
      'preferredMedium': ['', [
        Validators.required,
      ]],

      'lastName': [],
      'nic': [],
      'street': [],
      'province': [],
      'country': [],
    });

    this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    if (!this.userForm) {
      return;
    }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field) && (field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
                this.formErrors[field] += `${(messages as { [key: string]: string })[key]} `;
              }
            }
          }
        }
      }
    }
  }
}
