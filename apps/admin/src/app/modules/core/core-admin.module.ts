import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserLoginFormComponent} from "./components/user-login-form/user-login-form.component";
import {UserRegistrationFormComponent} from "./components/user-registration-form/user-registration-form.component";
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {UserProfileComponent} from "./components/user-profile/user-profile.component";
import {UserProfileSubIconComponent} from "./components/user-profile-subicon/user-profile-subicon.component";
import {MaterialModule} from "../../imports/material.module";
import {NotificationMessageComponent} from "./components/notification-message/notification-message.component";
import {LoadingSpinnerComponent} from "./components/loading-spinner/loading-spinner.component";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthService, BaseModule, PermissionsService, RolesService} from "@satipasala/base";
import {AutoCompleteSearchComponent} from './components/auto-complete-search/auto-complete-search.component';
import {CoreModule} from "@satipasala/core";
import {ConfirmationDialogComponent} from "./components/confirmation-dialog/confirmation-dialog.component";
import {UploadsModule} from "../uploads/uploads.module";

@NgModule({
  providers: [AngularFirestore, AuthService, RolesService],

  declarations: [
    UserLoginFormComponent, UserRegistrationFormComponent, UserProfileComponent, UserProfileSubIconComponent, NotificationMessageComponent, LoadingSpinnerComponent, AutoCompleteSearchComponent, ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MaterialModule, //todo remove unwanted material modules here
    CoreModule,
    BaseModule,
    MatDialogModule,
    UploadsModule
  ],

  exports: [
    UserLoginFormComponent, UserRegistrationFormComponent, UserProfileComponent, UserProfileSubIconComponent, NotificationMessageComponent, LoadingSpinnerComponent, AutoCompleteSearchComponent
  ]
})
export class CoreAdminModule {
}
