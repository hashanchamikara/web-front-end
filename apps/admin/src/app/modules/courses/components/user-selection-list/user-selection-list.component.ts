import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  Course,
  CourseSubscription,
  CourseSubscriptionService,
  ReferenceDataService, Role,
  User,
  UsersService
} from "@satipasala/base";
import {MatPaginator, MatSnackBar, PageEvent} from "@angular/material";
import {UserDataSource} from "./user-data-source";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatListOption} from "@angular/material/typings/list";
import {ObjectUtils} from "@satipasala/core";
import {UserInfo} from "../../../../../../../../libs/base/src/lib/model/User";
import * as APP_ROUTES from "../../../../app-routs";
@Component({
  selector: 'admin-user-selection-list',
  templateUrl: './user-selection-list.component.html',
  styleUrls: ['./user-selection-list.component.scss']
})
export class UserSelectionListComponent implements OnInit, OnDestroy, AfterViewInit {
  get course(): Course {
    return this._course;
  }

  @Input()
  set course(value: Course) {
    this._course = value;
  }

  private _course: Course;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  students: User[];

  dataSource: UserDataSource;
  //todo change this numbers to get from collection.
  length = 100;

  pageSize = 10;

  pageSizeOptions: number[] = [5, 10, 25, 100];
  courseStudentForm: FormGroup;

  selectedOrg: string;
  selectedYear: string;
  selectedGrade: string;
  selectedClass: string;
  selectedCourse: string;

  OrgList: string[];
  yearList: string[];
  gradeList: string[];
  classList: string[];

  constructor(private referenceDataService: ReferenceDataService,
              private usersService: UsersService,
              private courseSubscriptionService: CourseSubscriptionService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,) {

  }

  ngOnInit() {
    this.courseStudentForm = this.formBuilder.group({
      students: [null, Validators.compose([Validators.min(0)])]
    });

    this.dataSource = new UserDataSource(this.paginator, null, this.usersService);
    this.dataSource.connect().subscribe(users => {
      this.students = users;
    })
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect()
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  orgSelected() {
    //this.dataSource.addFilter({fieldPath: "uid", opStr:"!=",value:null});
  }

  yearSelected() {
    //this.dataSource.addFilter({fieldPath: "id"});
  }

  gradeSelected() {
    //this.dataSource.addFilter({fieldPath: "id"});
  }

  classSelected() {
    //  this.dataSource.addFilter({fieldPath: "id"});
  }

  isChecked(user: User) {
    if (user.courseSubscriptions) {
      return user.courseSubscriptions[user.uid+"_"+this.course.id]!=null;
    } else {
      return false
    }

  }

  onStudentsChange(student: User) {
    // this.courseStudentForm.patchValue({students: this.selectedStudents})
  }

  onSumbmit(selectedStudents: MatListOption[]) {
    selectedStudents.forEach(option => {
      let student: User = option.value;
      if (student.courseSubscriptions == null) {
        student.courseSubscriptions = {};
      }

      let courseSubscription = this.creteCourseSubscription(student);

      this.courseSubscriptionService.add(courseSubscription).then(value => {
        courseSubscription.id = value.id;
        this.courseNotification("Adding Course Subscription", "Success");
        this.addCourseSubscriptionToStudent(student, courseSubscription);
      }).catch(reason => {
        this.courseNotification("Adding Course Subscription", "Failed");
      })
    })
  }

  addCourseSubscriptionToStudent(student: User, courseSubscription: CourseSubscription) {
     student.courseSubscriptions[courseSubscription.userInfo.uid+"_"+courseSubscription.course.id]=courseSubscription;

      this.usersService.update(student.uid, student).then(value1 => {

        this.courseNotification("Adding Course Subscription to user", "Success");
      }).catch(reason => {

        this.courseNotification("Adding Course Subscription to User", "Failed");
      });

  }


  creteCourseSubscription(user: User): CourseSubscription {
    let courseSubscription = <CourseSubscription>{};
    courseSubscription.userInfo = ObjectUtils.reduceProperties(user, <UserInfo>{
      displayName: null,
      dob: null,
      email: null,
      firstName: null,
      id: null,
      lastName: null,
      nic: null,
      phoneNumber: null,
      photoURL: null,
      providerId: null,
      uid: null,
      userName: null,
      userRole: null
    });
    courseSubscription.course = this.course;

    return courseSubscription;
  }


  courseNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  backToCourseManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.COURSE_MANAGEMENT_ROUTE);
  }

}
