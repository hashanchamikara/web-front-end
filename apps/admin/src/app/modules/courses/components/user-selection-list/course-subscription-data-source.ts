import {CourseSubscription, CourseSubscriptionService, FirebaseDataSource, User, UsersService} from "@satipasala/base";
import {MatPaginator, MatSort} from "@angular/material";

export class CourseSubscriptionDataSource extends FirebaseDataSource<CourseSubscription>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public usersService: CourseSubscriptionService) {
    super(paginator, sort, usersService);
    this.setOrderBy({fieldPath:"user.uid"})
  }






}
