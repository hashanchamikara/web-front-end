import {FirebaseDataSource, User, UsersService} from "@satipasala/base";
import {MatPaginator, MatSort} from "@angular/material";

export class UserDataSource extends FirebaseDataSource<User>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public usersService: UsersService) {
    super(paginator, sort, usersService);
    this.setOrderBy({fieldPath:"uid"})
  }

}
