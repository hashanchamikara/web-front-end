import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar, MatSort, PageEvent} from '@angular/material';
import {ActivatedRoute, Router} from "@angular/router";
import {Course, CoursesService} from "@satipasala/base";
import * as APP_ROUTES from "../../../../app-routs";

@Component({
  selector: 'admin-course-activity-list',
  templateUrl: './course-activity-list.component.html',
  styleUrls: ['./course-activity-list.component.scss']
})
export class CourseActivityListComponent implements OnInit, OnDestroy {
  course: Course;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private coursesService: CoursesService,
    private _snackBar: MatSnackBar,) {
  }

  ngOnInit() {
    /*This form receives edit, view, new*/
    this.route.params.subscribe(params => {

      if (params.courseId) {
        this.coursesService.get(params.courseId).subscribe(course => {
            this.course = course;
          }, error => {
            this.courseNotification("Error retrieving course", "Redirecting...");
            setTimeout(() => {
              this.backToCourseManage();
            }, 1000)
          }
        )
      } else {
        // this.backToCourseManage();
      }
    });
  }

  courseNotification(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


  backToCourseManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.COURSE_MANAGEMENT_ROUTE);
  }

  ngAfterViewInit(): void {
  }

  loadMore(event: PageEvent) {
  }

  ngOnDestroy(): void {
  }
}
