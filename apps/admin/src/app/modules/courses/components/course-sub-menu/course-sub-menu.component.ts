import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Course, Host} from "@satipasala/base";

@Component({
  selector: 'admin-course-sub-menu',
  templateUrl: './course-sub-menu.component.html',
  styleUrls: ['./course-sub-menu.component.css']
})
export class CourseSubMenuComponent implements OnInit {
  @Input() course: Course;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }

  editCourse() {
    this.router.navigate([this.course.id + "/edit"], {relativeTo: this.activatedRoute});
  }

  viewCourse() {
    this.router.navigate([this.course.id + "/view"], {relativeTo: this.activatedRoute});
  }

  showActivites() {
    this.router.navigate(["activities/"+this.course.id ], {relativeTo: this.activatedRoute});
  }

  addStudents() {
    this.router.navigate(["course/assign/" + this.course.id], {relativeTo: this.activatedRoute});
  }
}
