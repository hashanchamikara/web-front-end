import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import * as APP_ROUTES from "../../../../app-routs";
import {
  Activity,
  Course,
  CoursesService,
  Questionnaire, QuestionnaireService, RefDataType,
  ReferenceDataService
} from "@satipasala/base";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'admin-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {

  static editMode: "edit" = "edit";
  static addMode: "add" = "add";
  mode: "edit" | "add";

  courseForm: FormGroup;

  public course: Course;

  public activityAll: Activity[] = [];

  public questionnaires: Questionnaire[] = [];

  public formTitle: string;
  public submitBtnText: string;

  get questionnaire(): string {
    if (this.course.questionnaire) {
      return this.course.questionnaire.name;
    } else {
      return null;
    }

  }

  set questionnaire(questionnaireName: string) {
    this.courseForm.patchValue({questionnaire: questionnaireName});
    this.course.questionnaire = this.questionnaires.filter(questionnaire => questionnaire.name === questionnaireName)[0]
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private coursesService: CoursesService,
    private _snackBar: MatSnackBar,
    private referenceDataService: ReferenceDataService,
    private questionnaireService:QuestionnaireService
  ) {
  }

  ngOnInit() {
    this.courseForm = this.formBuilder.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      facilitatorsCount: [null, Validators.compose([
        Validators.required, Validators.min(1), Validators.max(5)])
      ],
      numberOfFeedback: [null, Validators.compose([
        Validators.required, Validators.min(1), Validators.max(10)])
      ],
      activities: [null, Validators.compose([Validators.required, Validators.min(1)])],
      questionnaire: [null, Validators.required]
    });


    // Load all activities (ReferenceData here)
    this.referenceDataService.getData<Activity>(RefDataType.ACTIVITY).subscribe(activities => {
      this.activityAll = activities;
    });

   /* this.referenceDataService.getQuestionnaires().subscribe(questionnaires => {
      this.questionnaires = questionnaires;
    });*/

   this.questionnaireService .getAll().subscribe(questionnaires => {
     this.questionnaires = questionnaires;
   })



    /*This form receives edit, view, new*/
    this.route.params.subscribe(params => {

      if (params.courseId === 'new') {
        this.mode = CourseFormComponent.addMode;
        this.formTitle = "Create Course";
        this.submitBtnText = "Add";
        let course = <Course>{
          activities: [],
          facilitatorsCount: 0
        };
        this.fillForm(course);
      } else if (params.action === 'edit') {
        this.mode = CourseFormComponent.editMode;
        this.formTitle = "Edit Course";
        this.submitBtnText = "Update";
        this.coursesService.get(params.courseId).subscribe(course => {
            this.fillForm(course);
          }, error => {
            this.courseNotification("Error retrieving course", "Redirecting...");
            setTimeout(() => {
              this.backToCourseManage();
            }, 1000)
          }
        )
      } else {
        this.backToCourseManage();
      }
    });
  }

  fillForm(course: Course) {
    this.course = course;
    this.courseForm.patchValue({
      name: this.course.name,
      description: this.course.description,
      facilitators: this.course.facilitatorsCount,
      numberOfFeedback: this.course.numberOfFeedback,
      activities: this.course.activities,
    });

    if (this.course.questionnaire) {
      this.courseForm.patchValue({
        questionnaire: this.course.questionnaire.name
      });
    }
  }


  isChecked(activityName) {
    //toto check id instead of name
    if (this.course != null && this.course.activities != null)
      return this.course.activities.findIndex(activity => activityName === activity.name)>-1;
    else
      return false;
  }

  onActivitiesChange($event) {
    this.courseForm.patchValue({activities: this.course.activities})
  }


  onSubmit() {
    //this.question = Object.assign(this.question, values);
    switch (this.mode) {
      case CourseFormComponent.addMode:
        this.addCourse();
        break;
      case CourseFormComponent.editMode:
        this.updateCourse();
        break;
    }
  }

  updateCourse() {
    this.coursesService.update(this.course.id, this.course).then(value => {
      this.courseNotification("Course Update", "Success");
    }).catch(reason => {
      console.error(reason);
      this.courseNotification("Course Update", "Failed!!!");
    });
  }

  addCourse() {
    this.coursesService.add(this.course).then(value => {
      this.courseNotification("Course add", "Success");
    }).catch(reason => {
      console.error(reason);
      this.courseNotification("Course add", "Failed!!!");
    })
  }

  courseNotification(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


  backToCourseManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.COURSE_MANAGEMENT_ROUTE);
  }

}
