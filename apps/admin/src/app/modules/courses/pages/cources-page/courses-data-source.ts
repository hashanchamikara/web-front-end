import {FirebaseDataSource} from "@satipasala/base";
import {Course} from "@satipasala/base";
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {CoursesService} from "@satipasala/base";

export class CoursesDataSource extends FirebaseDataSource<Course>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public coursesService: CoursesService) {
    super(paginator, sort, coursesService);
    this.setOrderBy({fieldPath:"name"})
  }

}
