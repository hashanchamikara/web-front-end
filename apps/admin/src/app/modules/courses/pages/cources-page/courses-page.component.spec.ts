import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSortModule, MatTableModule, MatMenuModule, MatIconModule } from '@angular/material';

import { CoursesPage } from './courses-page.component';
import { CourseSubMenuComponent } from '../../components/course-sub-menu/course-sub-menu.component';
import {CoursesService} from "@satipasala/base";
import {AngularFirestore} from "@angular/fire/firestore";
import {MockFireStore} from "@satipasala/testing";

describe('CoursesPage', () => {
  let component: CoursesPage;
  let fixture: ComponentFixture<CoursesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[CoursesService, {provide: AngularFirestore, useValue: MockFireStore}],
      declarations: [ CoursesPage, CourseSubMenuComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        MatMenuModule,
        MatIconModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
