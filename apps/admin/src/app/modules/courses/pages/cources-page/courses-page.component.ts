import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {CoursesService} from "@satipasala/base";
import {CoursesDataSource} from "./courses-data-source";
import {COURSE_MANAGEMENT_ACTION_ROUTE, QUESTIONNAIRE_MANAGEMENT_ADD_ROUTE} from "../../../../app-routs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'admin-cources-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: CoursesDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','facilitatorsCount','numberOfFeedback','questionnaireName','description','edit'];

  constructor(private router:Router,private  activatedRoute: ActivatedRoute, private  coursesService:CoursesService){

  }

  ngOnInit() {
    this.dataSource = new CoursesDataSource(this.paginator, this.sort, this.coursesService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  addNewCourse() {
    this.router.navigate(["new/add"], {relativeTo: this.activatedRoute});
  }
}
