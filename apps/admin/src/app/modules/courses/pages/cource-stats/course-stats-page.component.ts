import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatListOption, MatPaginator, MatSelectionListChange, MatSnackBar, PageEvent} from "@angular/material";
import * as _ from "lodash";
import {
  Course, CourseSubscription,
  Questionnaire,
  ReferenceDataService,
  User,
  UsersService,

} from "@satipasala/base";
import {UserDataSource} from "../../components/user-selection-list/user-data-source";
import {Occurrence} from "../../../../../../../../libs/base/src/lib/model/Occurrence";
import {Feedback} from "../../../../../../../../libs/base/src/lib/model/Feedback";
import {FeedbackService} from "../../../../../../../../libs/base/src/lib/services/feedback.service";
import { SearchFilter } from '../../../../../../../../libs/base/src/lib/model/SearchFilter';

@Component({
  selector: 'admin-cource-stats',
  templateUrl: './course-stats-page.component.html',
  styleUrls: ['./course-stats-page.component.scss']
})
export class CourseStatsPage implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  feedbackForm: FormGroup;
  feedback: Feedback;
  formTitle: string = "Feedback Form";
  selectedStudent: User;
  students: User[];
  courses: Course[] = [];
  submitBtnText: string = "Submit";
  currentYear: string;
  selectedCourse: Course;
  dataSource: UserDataSource;

  //Variables for the search component
  collection: string = 'users';
  displayField: string = 'displayName';
  searchFields: string[] = ['displayName', 'id'];

  occurrences: Occurrence[] = [{number:1, name:"start"}, {number:2, name:"mid"}, {number:3, name:"end"}];
  selectedOccurrence: Occurrence;

  selectedQuestionnaire: Questionnaire;

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];


  constructor(private referenceDataService: ReferenceDataService,
              private feedbackService: FeedbackService,
              private usersService: UsersService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackBar: MatSnackBar,
              private formBuilder: FormBuilder,) {
  }

  ngOnInit() {
    this.setCurrentYear();
    this.setSearchFilters();
    //Init FormGroup
    this.feedbackForm = this.formBuilder.group({
      organisation: '',
      year: [{value: this.currentYear, disabled: true}],
      course: '',
      occurrence: '',
      questionnaireType: ''
    });

    this.dataSource = new UserDataSource(this.paginator,null, this.usersService);
    this.dataSource.connect().subscribe(users => {
      this.students = users;
    })
  }

  setSearchFilters() {
    }

  ngOnDestroy(): void {
    this.dataSource.disconnect()
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  private setCurrentYear() {
    this.currentYear = new Date().getFullYear().toLocaleString();
  }

  selectStudent($event: MatSelectionListChange, selected: MatListOption[]) {
    //Allow only one student to be selected at a time
    if($event.option.selected){
      $event.source.deselectAll();
      $event.option._setSelected(true);
    }
    //Handle selected Student
    this.resetStudentSpecificData();
    //Handle instances where no student is selected
    if(selected.length == 0){
      console.log("Nothing selected");
      this.courses = [];
      return;
    }
    this.selectedStudent = selected.pop().value;
    this.setCourses();
  }

  resetStudentSpecificData() {
    this.selectedCourse = null;
    this.selectedOccurrence = null;
    this.selectedQuestionnaire = null
  }

  private setCourses() {
    //Clear existing enrollments
    this.courses = [];
    //Populate the enrollments array
    if(this.selectedStudent.courseSubscriptions != null){
      for(let e in this.selectedStudent.courseSubscriptions){
        this.courses.push(this.selectedStudent.courseSubscriptions[e].course);
      }
    }
  }

  addEditFeedback() {
    this.feedback = <Feedback>{};
    this.feedback.course = this.selectedCourse;
    this.feedback.feedback = this.selectedQuestionnaire;
    this.feedback.occurrence = this.selectedOccurrence;
    this.feedback.userInfo = this.selectedStudent.courseSubscriptions[Object.keys(this.selectedStudent.courseSubscriptions)[0]].userInfo;
    console.log(this.feedback);
    this.feedbackService.add(this.feedback)
      .then(() => {
        alert("Feedback Added")
      }).catch(
        err => console.log(err)
    );
  }

  clearForm() {

  }

  courseSelected() {
    // console.log(this.selectedCourse)
    this.selectedQuestionnaire = _.cloneDeep(this.selectedCourse.questionnaire)
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  onQuestionnaireSubmit() {
    console.log(this.selectedQuestionnaire);
  }
}
