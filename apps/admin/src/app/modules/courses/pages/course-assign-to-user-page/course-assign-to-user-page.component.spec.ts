import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseAssignToUserPage } from './course-assign-to-user-page.component';

describe('CourseAssignToUserPageComponent', () => {
  let component: CourseAssignToUserPage;
  let fixture: ComponentFixture<CourseAssignToUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseAssignToUserPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseAssignToUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
