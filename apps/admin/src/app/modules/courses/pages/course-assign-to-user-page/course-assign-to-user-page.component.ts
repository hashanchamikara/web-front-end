import { Component, OnInit } from '@angular/core';
import {Course, CoursesService} from "@satipasala/base";
import {ActivatedRoute, Route, Router} from "@angular/router";
import * as APP_ROUTES from "../../../../app-routs";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'admin-course-assign-to-user-page',
  templateUrl: './course-assign-to-user-page.component.html',
  styleUrls: ['./course-assign-to-user-page.component.scss']
})
export class CourseAssignToUserPage implements OnInit {
  course:Course;

  constructor(
    private router: Router,
    private route: ActivatedRoute, private coursesService:CoursesService,private snackBar: MatSnackBar,) {

    /*This form receives edit, view, new*/
    this.route.params.subscribe(params => {
        this.coursesService.get(params.courseId).subscribe(course => {
            this.course = course;
          }, error => {
            this.courseNotification("Error retrieving course", "Redirecting...");
            setTimeout(() => {
              this.backToCourseManage();
            }, 1000)
          }
        )
    });

  }

  courseNotification(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  backToCourseManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.COURSE_MANAGEMENT_ROUTE);
  }

  ngOnInit() {
  }

}
