import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CoursesPage} from './pages/cources-page/courses-page.component';
import {EditCoursePage} from './pages/course-edit-page/edit-course-page.component';
import {
  COURSE_MANAGEMENT_ACTION_ROUTE,
  COURSE_ACTIVITY_INFO_ROUTE,
  COURSE_FEEDBACK_ROUTE, COURSE_ASSIGN_TO_USER_ROUTE
} from '../../app-routs';
import {CourseStatsPage} from "./pages/cource-stats/course-stats-page.component";
import {CourseAssignToUserPage} from "./pages/course-assign-to-user-page/course-assign-to-user-page.component";
import {CourseActivityListComponent} from "./components/course-activity-list/course-activity-list.component";

const routes: Routes = [
  {path: '', component: CoursesPage},

  {path: COURSE_ASSIGN_TO_USER_ROUTE, component: CourseAssignToUserPage},
  {path: COURSE_ACTIVITY_INFO_ROUTE, component: CourseActivityListComponent},
  {path: COURSE_FEEDBACK_ROUTE, component: CourseStatsPage},
  {path: COURSE_MANAGEMENT_ACTION_ROUTE, component: EditCoursePage}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {
}
