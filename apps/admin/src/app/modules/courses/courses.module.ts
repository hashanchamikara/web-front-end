import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesPage } from './pages/cources-page/courses-page.component';
import { CourseSubMenuComponent } from './components/course-sub-menu/course-sub-menu.component';
import { EditCoursePage } from './pages/course-edit-page/edit-course-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CourseActivityListComponent } from './components/course-activity-list/course-activity-list.component';
import { CourseActivitySubMenuComponent } from './components/course-activity-sub-menu/course-activity-sub-menu.component';
import {BaseModule, CoursesService, UsersService} from "@satipasala/base";
import {CourseFormComponent} from "./components/course-form/course-form.component";
import {CoreModule, ErrorStateMatcherFactory} from "@satipasala/core";
import {MaterialModule} from "../../imports/material.module";
import {CourseStatsPage} from "./pages/cource-stats/course-stats-page.component";
import { CourseAssignToHostPageComponent } from './pages/course-assign-to-host-page/course-assign-to-host-page.component';
import { CourseAssignToUserPage } from './pages/course-assign-to-user-page/course-assign-to-user-page.component';
import { UserSelectionListComponent } from './components/user-selection-list/user-selection-list.component';

@NgModule({
  declarations: [CoursesPage, CourseStatsPage, CourseSubMenuComponent, EditCoursePage,CourseActivityListComponent,CourseActivitySubMenuComponent,
    CourseFormComponent, CourseAssignToHostPageComponent, CourseAssignToUserPage, UserSelectionListComponent],
  imports: [
    CommonModule,
    CoursesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    BaseModule
  ],
  providers:[CoursesService,ErrorStateMatcherFactory,UsersService]
})
export class CoursesModule { }
