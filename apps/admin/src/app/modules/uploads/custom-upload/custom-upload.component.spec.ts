import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomUploadComponent } from './custom-upload.component';
import {FileUploadComponent} from "../file-upload/file-upload.component";
import {ActivatedRoute} from "@angular/router";
import {FileUploadModule} from "ng2-file-upload";
import {MatButtonModule, MatCardModule, MatIconModule, MatProgressBarModule} from "@angular/material";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {UploadTaskComponent} from "../upload-task/upload-task.component";

describe('CustomUploadComponent', () => {
  let component: CustomUploadComponent;
  let fixture: ComponentFixture<CustomUploadComponent>;
  const fakeActivatedRoute = {
    snapshot: { data: { } }
  } as ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{provide: ActivatedRoute, useValue: fakeActivatedRoute}],
      declarations: [ CustomUploadComponent, FileUploadComponent, UploadTaskComponent ],
      imports:[
        FileUploadModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatCardModule,
        MatIconModule,
        MatProgressBarModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
