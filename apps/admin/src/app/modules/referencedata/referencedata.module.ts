import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "../../imports/material.module";
import {ReferenceDataRoutingModule} from "./referencedata-routing.module";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {ActivityTypesComponent} from "./components/type/activitytypes/activitytypes.component";
import {RefDataContextMenuComponent} from "./components/base";
import {ActivityTypeFormComponent} from "./components/type/activitytypes/activity-type-form/activity-type-form.component";
import {QuestionTypeFormComponent} from "./components/type/questiontypes/question-type-form/question-type-form.component";
import {QuestionTypesComponent} from "./components/type/questiontypes/questiontypes.component";
import {ActivityComponent} from "./components/type/activities/activity.component";
import {ActivityFormComponent} from "./components/type/activities/activity-form/activity-form.component";
import {ConfirmationDialogComponent} from "../core";
import {CoreAdminModule} from "../core/core-admin.module";

@NgModule({
  declarations: [ActivityTypesComponent, RefDataContextMenuComponent, ActivityTypeFormComponent, QuestionTypesComponent, QuestionTypeFormComponent, ActivityTypeFormComponent, ActivityTypesComponent, ActivityFormComponent, ActivityComponent],
  imports: [
    CommonModule,
    ReferenceDataRoutingModule,
    MaterialModule,
    AngularFirestoreModule,
    CoreAdminModule
  ],
  entryComponents: [ActivityTypeFormComponent, ConfirmationDialogComponent, QuestionTypeFormComponent, ActivityFormComponent]
})
export class ReferenceDataModule {
}
