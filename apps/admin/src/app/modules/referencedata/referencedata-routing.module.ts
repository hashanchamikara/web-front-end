import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
  REFERENCE_DATA_ACTIVITY_ROUTE,
  REFERENCE_DATA_ACTIVITY_TYPE_ROUTE,
  REFERENCE_DATA_QUESTION_TYPE_ROUTE,
  REFERENCE_DATA_ROUTE
} from "../../app-routs";
import {ActivityTypesComponent} from "./components/type/activitytypes/activitytypes.component";
import {QuestionTypesComponent} from "./components/type/questiontypes/questiontypes.component";
import {ActivityComponent} from "./components/type/activities/activity.component";


const routes: Routes = [
  {path: '', redirectTo: REFERENCE_DATA_ROUTE, pathMatch: 'full'},
  {path: REFERENCE_DATA_ACTIVITY_TYPE_ROUTE, component: ActivityTypesComponent},
  {path: REFERENCE_DATA_ACTIVITY_ROUTE, component: ActivityComponent},
  {path: REFERENCE_DATA_QUESTION_TYPE_ROUTE, component: QuestionTypesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferenceDataRoutingModule {
}
