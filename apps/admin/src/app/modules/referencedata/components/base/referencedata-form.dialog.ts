import {OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {FormGroup} from "@angular/forms";


export abstract class ReferenceDataFormDialog<T, C> implements OnInit {
  public formGroup: FormGroup;

  protected constructor(public dialogRef: MatDialogRef<C>, public  dataObj: RefDataFormObject<T>) {
    dialogRef.afterClosed().subscribe(observer => this.onClose());
  }

  ngOnInit(): void {
    this.formGroup = this.getFormGroup();

    // Disable key field for edit mode
    if (this.dataObj.type === ViewType.EDIT) {
      this.formGroup.controls['name'].disable();
    }

    // Disable form for view mode
    if (this.dataObj.type === ViewType.VIEW) {
      this.formGroup.disable();
    }

    this.afterInit();
  }

  onSave(): void {
    if (this.formGroup.valid) {
      this.dialogRef.close(this.formGroup.getRawValue());
    } else {
      alert("Errors found! Please check and try again. ")
    }
  }

  cancel(): void {
    this.dialogRef.close();
  }

  abstract getFormGroup(): FormGroup;

  afterInit(): void {
  }

  onClose(): void{
  }

}

export class RefDataFormObject<T> {
  public type: ViewType;
  public data: T;

  constructor(type: ViewType, data: T) {
    this.type = type;
    this.data = data;
  }
}

export enum ViewType {
  CREATE = "create",
  VIEW = "view",
  EDIT = "edit",
  DELETE = "delete"
}
