import {AfterViewInit, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {MatDialog} from '@angular/material/dialog';
import {AngularFirestore} from "@angular/fire/firestore";
import {QuestionnaireService, RefDataType, ReferenceDataService} from "@satipasala/base";
import {MatDialogRef} from "@angular/material/dialog/typings/dialog-ref";
import {RefDataContextMenuItem, RefDataContextMenuType} from "./context-menu/context-menu.component";
import {ComponentType} from "@angular/cdk/portal";
import {ReferenceDataTableDatasource} from "./referencedata-table.datasource";
import {RefDataFormObject, ViewType} from "./referencedata-form.dialog";
import {ConfirmationDialogComponent, ConfirmDialogModel} from "../../../core";

export abstract class RefDataTypeComponent<T> implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: true})
  paginator: MatPaginator;

  @ViewChild(MatSort, {static: true})
  sort: MatSort;

  public dataSource: ReferenceDataTableDatasource<T>;
  public pageEvent: PageEvent;

  constructor(protected refDataType: RefDataType, public title: string, protected fireStore: AngularFirestore,
              protected dialog: MatDialog, protected referenceDataService: ReferenceDataService) {
  }

  /**
   * Display column array of the grid
   *
   * @returns {string[]}
   */
  abstract displayedColumns(): string[];

  /**
   *  Add/View/Edit form minimum width
   *
   * @returns {string}
   */
  abstract refDataFormMinWidth(): string;

  /**
   * New object for create action
   *
   * @returns {T}
   */
  abstract createNewObject(): T;

  /**
   * Component type for Add/View/Edit form dialog
   *
   * @returns {ComponentType<C>}
   */
  abstract getComponentType<C>(): ComponentType<C>;


  ngOnInit() {
    this.dataSource = new ReferenceDataTableDatasource<T>(this.paginator, this.sort, this.fireStore, this.refDataType);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  /**
   * Add new record
   */
  addNew(): void {
    this.openRefDataFormDialog(this.refDataFormMinWidth(), new RefDataFormObject(ViewType.CREATE, this.createNewObject())).afterClosed().subscribe(result => {
      if (result) {
        this.referenceDataService.mergeData(this.refDataType, result); //DB merge
      }
    });
  }

  /**
   * Perform context menu action
   *
   * @param {RefDataContextMenuItem<T>} contextMenuItem
   */
  onContextMenuCommand(contextMenuItem: RefDataContextMenuItem<any>): void {
    switch (contextMenuItem.type) {
      case RefDataContextMenuType.VIEW:
        this.openRefDataFormDialog(this.refDataFormMinWidth(), contextMenuItem);
        break;
      case RefDataContextMenuType.EDIT:
        this.openRefDataFormDialog(this.refDataFormMinWidth(), contextMenuItem).afterClosed().subscribe(result => {
          if (result) {
            this.referenceDataService.mergeData(this.refDataType, result);//DB merge
          }
        });
        break;
      case RefDataContextMenuType.DELETE:
        const dialogData = new ConfirmDialogModel("Confirm Action", `Are you sure you want to delete "${contextMenuItem.data.name}"?`);
        this.dialog.open(ConfirmationDialogComponent, {
          minWidth: "300px",
          data: dialogData
        }).afterClosed().subscribe(result => {
          if (result) {
            this.referenceDataService.removeData(this.refDataType, contextMenuItem.data); //DB remove
          }
        });
        break;
    }
  }

  /**
   * Open form dialog
   *
   * @param {string} minWidth
   * @param data
   * @returns {MatDialogRef<any>}
   */
  openRefDataFormDialog(minWidth: string, data: any): MatDialogRef<any> {
    return this.dialog.open(this.getComponentType(), {
      minWidth: minWidth,
      data: data
    });
  }


}
