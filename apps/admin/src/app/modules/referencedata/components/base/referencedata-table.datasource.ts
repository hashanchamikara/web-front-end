import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {AfterViewInit, OnDestroy} from "@angular/core";
import {FirebaseDataSource, QuestionnaireService} from "@satipasala/base";
import {RefDataType, ReferenceDataService} from "@satipasala/base";
import {AngularFirestore} from "@angular/fire/firestore";
import {Subscription} from "rxjs";

export class ReferenceDataTableDatasource<T> extends FirebaseDataSource<T> implements AfterViewInit, OnDestroy {
  private paginator: MatPaginator;
  private sort: MatSort;
  private referenceDataService: ReferenceDataService;
  private refDataSubscription: Subscription;

  constructor(paginator: MatPaginator, sort: MatSort, fireStore: AngularFirestore, private dataType: RefDataType) {
    super(paginator, sort, new ReferenceDataService(fireStore));
    this.paginator = paginator;
    this.sort = sort;
    this.referenceDataService = this.collectionService as ReferenceDataService;
  }

  loadMore(event: PageEvent) {
    console.log("Not implemented!"); //TODO implement pagination
    return;
    // this.queryData(query => query.orderBy("name").startAt(event.pageIndex).limit(event.pageSize), {
    //   documentId: this.locale,
    //   subCollection: 'activityTypes'
    // });
  }

  fetchData(): void {
    this.refDataSubscription = this.referenceDataService.getData(this.dataType).subscribe(refDataArray => {
      this.dataSubject.next(refDataArray as T[]);
    });
  }

  ngOnDestroy() {
    this.refDataSubscription.unsubscribe();
  }
}
