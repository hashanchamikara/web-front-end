import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'admin-ref-data-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.css']
})
export class RefDataContextMenuComponent<T> implements OnInit {

  @Input()
  private refDataObj: T;

  @Output()
  private contextMenuClickEmitter: EventEmitter<RefDataContextMenuItem<T>> = new EventEmitter<RefDataContextMenuItem<T>>();

  constructor() {
  }

  ngOnInit() {
  }

  view() {
    this.contextMenuClickEmitter.emit(new RefDataContextMenuItem(RefDataContextMenuType.VIEW, this.refDataObj));
  }

  edit() {
    this.contextMenuClickEmitter.emit(new RefDataContextMenuItem(RefDataContextMenuType.EDIT, this.refDataObj));
  }

  delete() {
    this.contextMenuClickEmitter.emit(new RefDataContextMenuItem(RefDataContextMenuType.DELETE, this.refDataObj));
  }

}

/**
 * Reference data context menu object
 *
 */
export class RefDataContextMenuItem<T> {
  public type: RefDataContextMenuType;
  public data: T;

  constructor(type: RefDataContextMenuType, data: T) {
    this.type = type;
    this.data = data;
  }
}

/**
 * Reference data types
 *
 */
export enum RefDataContextMenuType {
  VIEW = "view",
  EDIT = "edit",
  DELETE = "delete"
}
