import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AngularFirestore} from "@angular/fire/firestore";
import {ActivityTypeFormComponent,} from "./activity-type-form/activity-type-form.component";
import {ActivityType, RefDataType, ReferenceDataService} from "@satipasala/base";
import {ComponentType} from "@angular/cdk/portal";
import {RefDataTypeComponent} from "../../base";

@Component({
  selector: 'admin-activity-types',
  templateUrl: './activitytypes.component.html',
  styleUrls: ['./activitytypes.component.css']
})
export class ActivityTypesComponent extends RefDataTypeComponent<ActivityType> {

  constructor(fireStore: AngularFirestore, dialog: MatDialog, referenceDataService: ReferenceDataService) {
    super(RefDataType.ACTIVITY_TYPE, "Activity Types", fireStore, dialog, referenceDataService);
  }

  refDataFormMinWidth(): string {
    return "300px";
  }

  createNewObject(): ActivityType {
    return new ActivityType();
  }

  getComponentType(): ComponentType<any> {
    return ActivityTypeFormComponent;
  }

  displayedColumns(): string[] {
    return ['name', 'description', 'active', 'edit'];
  }

}
