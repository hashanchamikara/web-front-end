import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RefDataFormObject, ReferenceDataFormDialog} from "../../../base";
import {QuestionType} from "@satipasala/base";

@Component({
  selector: 'admin-question-type-form',
  templateUrl: './question-type-form.component.html',
  styleUrls: ['./question-type-form.component.css']
})
export class QuestionTypeFormComponent extends ReferenceDataFormDialog<QuestionType, QuestionTypeFormComponent> {


  constructor(public fb: FormBuilder, dialogRef: MatDialogRef<QuestionTypeFormComponent>, @Inject(MAT_DIALOG_DATA) formObject: RefDataFormObject<QuestionType>) {
    super(dialogRef, formObject);
  }

  getFormGroup(): FormGroup {
    return this.fb.group({
      id: [this.dataObj.data.id || "", Validators.required],
      name: [this.dataObj.data.name || "", Validators.required],
      active: [this.dataObj.data.active || "Yes"],
      type: [this.dataObj.data.type || ""],
      label: [this.dataObj.data.label || ""],
      fieldType: [this.dataObj.data.fieldType || ""],
      answerType: [this.dataObj.data.answerType || ""]
    });
  }

}

