import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AngularFirestore} from "@angular/fire/firestore";
import {QuestionType, RefDataType, ReferenceDataService} from "@satipasala/base";
import {ComponentType} from "@angular/cdk/portal";
import {RefDataTypeComponent} from "../../base";
import {QuestionTypeFormComponent} from "./question-type-form/question-type-form.component";

@Component({
  selector: 'admin-question-types',
  templateUrl: './questiontypes.component.html',
  styleUrls: ['./questiontypes.component.css']
})
export class QuestionTypesComponent extends RefDataTypeComponent<QuestionType> {

  constructor(fireStore: AngularFirestore, dialog: MatDialog, referenceDataService: ReferenceDataService) {
    super(RefDataType.QUESTION_TYPE, "Question Types", fireStore, dialog, referenceDataService);
  }

  refDataFormMinWidth(): string {
    return "300px";
  }

  createNewObject(): QuestionType {
    return new QuestionType();
  }

  getComponentType(): ComponentType<any> {
    return QuestionTypeFormComponent;
  }

  displayedColumns(): string[] {
    return ['id', 'name', 'type', 'label', 'fieldType', 'answerType', 'active', 'edit'];
  }

}
