import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RefDataFormObject, ReferenceDataFormDialog} from "../../../base";
import {Activity, ActivityType, RefDataType, ReferenceDataService} from "@satipasala/base";
import {Subscription} from "rxjs";

@Component({
  selector: 'admin-activity-form',
  templateUrl: './activity-form.component.html',
  styleUrls: ['./activity-form.component.css']
})
export class ActivityFormComponent extends ReferenceDataFormDialog<Activity, ActivityFormComponent> {
  public activityTypes: ActivityType[];
  public activityTypeObservable: Subscription;

  constructor(private fb: FormBuilder, dialogRef: MatDialogRef<ActivityFormComponent>, @Inject(MAT_DIALOG_DATA) formObject: RefDataFormObject<Activity>, private referenceDataService: ReferenceDataService) {
    super(dialogRef, formObject);
  }

  getFormGroup(): FormGroup {
    return this.fb.group({
      name: [this.dataObj.data.name || "", Validators.required],
      active: [this.dataObj.data.active || "Yes"],
      type: [this.dataObj.data.type || "", Validators.required],
      description: [this.dataObj.data.description || ""],
      maxPoints: [this.dataObj.data.maxPoints || 25],
      gradable: [this.dataObj.data.gradable || "Yes"]
    });
  }

  afterInit(): void {
    this.activityTypeObservable = this.referenceDataService.getData(RefDataType.ACTIVITY_TYPE).subscribe(activityTypeArr => {
      this.activityTypes = activityTypeArr;
      //Set activity type object as in the options list
      this.formGroup.controls['type'].setValue(activityTypeArr.find(type => (type.name == this.dataObj.data.type.name)));
    });
  }

  onClose(): void {
    this.activityTypeObservable.unsubscribe();
  }

}

