import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {RefDataFormObject, ReferenceDataFormDialog} from "../../../base";
import {ActivityType} from "@satipasala/base";

@Component({
  selector: 'admin-activity-type-form',
  templateUrl: './activity-type-form.component.html',
  styleUrls: ['./activity-type-form.component.css']
})
export class ActivityTypeFormComponent extends ReferenceDataFormDialog<ActivityType, ActivityTypeFormComponent> {


  constructor(public fb: FormBuilder, dialogRef: MatDialogRef<ActivityTypeFormComponent>, @Inject(MAT_DIALOG_DATA) formObject: RefDataFormObject<ActivityType>) {
    super(dialogRef, formObject);
  }

  getFormGroup(): FormGroup {
    return this.fb.group({
      name: [this.dataObj.data.name || "", Validators.required],
      active: [this.dataObj.data.active || "Yes"],
      description: [this.dataObj.data.description || ""]
    });
  }

}

