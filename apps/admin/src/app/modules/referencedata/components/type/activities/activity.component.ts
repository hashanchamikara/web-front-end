import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AngularFirestore} from "@angular/fire/firestore";
import {Activity, RefDataType, ReferenceDataService} from "@satipasala/base";
import {ComponentType} from "@angular/cdk/portal";
import {RefDataTypeComponent} from "../../base";
import {ActivityFormComponent} from "./activity-form/activity-form.component";

@Component({
  selector: 'admin-activities',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent extends RefDataTypeComponent<Activity> {

  constructor(fireStore: AngularFirestore, dialog: MatDialog, referenceDataService: ReferenceDataService) {
    super(RefDataType.ACTIVITY, "Activities", fireStore, dialog, referenceDataService);
  }

  refDataFormMinWidth(): string {
    return "300px";
  }

  createNewObject(): Activity {
    return new Activity();
  }

  getComponentType(): ComponentType<any> {
    return ActivityFormComponent;
  }

  displayedColumns(): string[] {
    return ['name', 'type', 'description', 'maxPoints', 'gradable', 'active', 'edit'];
  }

}
