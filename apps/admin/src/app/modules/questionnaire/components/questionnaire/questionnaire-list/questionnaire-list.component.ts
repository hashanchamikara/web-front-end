import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionnaireService} from "@satipasala/base";
import {QuestionnaireDataSource} from "./questionnaire-data-source";
import {QUESTION_MANAGEMENT_ADD_ROUTE, QUESTIONNAIRE_MANAGEMENT_ADD_ROUTE} from "../../../../../app-routs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'admin-questionnaire-list',
  templateUrl: './questionnaire-list.component.html',
  styleUrls: ['./questionnaire-list.component.scss']
})
export class QuestionnaireListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: QuestionnaireDataSource;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id','name', 'edit'];
  constructor(private router:Router, private  questionnaireService:QuestionnaireService, private activatedRoute:ActivatedRoute){

  }

  ngOnInit() {
    this.dataSource = new QuestionnaireDataSource(this.paginator, this.sort, this.questionnaireService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  addNewQuestionnaire() {
    this.router.navigate([QUESTIONNAIRE_MANAGEMENT_ADD_ROUTE], {relativeTo: this.activatedRoute});
  }
}
