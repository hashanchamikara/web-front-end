import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Question, Questionnaire} from "@satipasala/base";
import {QuestionnaireService, QuestionsService, ReferenceDataService} from "@satipasala/base";
import {QuestionsDragDropDataSource} from "./questions-drag-drop-data-source";
import {MatPaginator, MatSnackBar, PageEvent} from "@angular/material";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {QuestionLevel} from "../../../../../../../../../libs/base/src/lib/model/Question";
import {QUESTION_MANAGEMENT_ADD_ROUTE} from "../../../../../app-routs";
import {ActivatedRoute, Router} from "@angular/router";
import {Chip, ChipList} from "@satipasala/core";

@Component({
  selector: 'admin-questions-drag-drop',
  templateUrl: './questions-drag-drop.component.html',
  styleUrls: ['./questions-drag-drop.component.scss']
})
export class QuestionsDragDropComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() questionnaire: Questionnaire;
  @Input() connectedTo:string[]=[]
  @ViewChild('dragDropList',{static:true}) dragDropList: ElementRef;

  questionLevelsChipList: ChipList = new class implements ChipList {
    stacked: false;
    type: "Question Types";
    values: QuestionLevel[];
  }

  questions: Question<any>[] =[];

  dataSource: QuestionsDragDropDataSource;
  //todo change this numbers to get from collection.
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private referenceDataService: ReferenceDataService,
              private questionsService: QuestionsService,
              private questionnaireService: QuestionnaireService,
              private router:Router,
              private activatedRoute:ActivatedRoute,
              private _snackBar: MatSnackBar) {

  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  ngOnInit() {
    this.questionLevelsChipList.values = this.referenceDataService.getQuestionLevels();
    this.dataSource = new QuestionsDragDropDataSource(this.paginator, null, this.questionsService);
    this.dataSource.connect().subscribe(questions => {
      this.questions = this.filterUnassignedQuestions(questions);
    })
  }

  filterUnassignedQuestions(questions:Question<any>[]):Question<any>[]{
   return questions.filter(question => {
      return this.questionnaire.questions.filter(q => {return q.id === question.id;}).length===0
    })
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect()
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  onQuestionTypeClick(chip: Chip) {
    console.log(chip);
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  addNewQuestion() {
    this.router.navigate([QUESTION_MANAGEMENT_ADD_ROUTE], {relativeTo: this.activatedRoute});
  }

}
