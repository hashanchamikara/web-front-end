import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {
  HOST_MANAGEMENT_ROUTE,
  QUESTIONNAIRE_MANAGEMENT_EDIT_ROUTE, QUESTIONNAIRE_MANAGEMENT_INFO_ROUTE, QUESTIONNAIRE_MANAGEMENT_ROUTE,
} from "../../../../../app-routs";
import {Questionnaire} from "@satipasala/base";

@Component({
  selector: 'admin-questionnaire-sub-menu',
  templateUrl: './questionnaire-sub-menu.component.html',
  styleUrls: ['./questionnaire-sub-menu.component.css']
})
export class QuestionnaireSubMenuComponent implements OnInit {
  @Input() questionnaire: Questionnaire;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  editQuestionnaire() {
    //this.router.navigate([QUESTIONNAIRE_MANAGEMENT_ROUTE + "/edit/" + this.questionnaire.id]);
    this.router.navigateByUrl(QUESTIONNAIRE_MANAGEMENT_ROUTE + "/edit/" + this.questionnaire.id);
  }

  viewQuestionnaire() {
    this.router.navigate([QUESTIONNAIRE_MANAGEMENT_ROUTE + "/info/" + this.questionnaire.id]);
    /*,relativeTo: this.activatedRoute*/
  }
}
