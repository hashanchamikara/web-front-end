import {Component, Input, OnInit} from '@angular/core';
import {Questionnaire} from "@satipasala/base";

@Component({
  selector: 'admin-questionnaire-stepper',
  templateUrl: './questionnaire-stepper.component.html',
  styleUrls: ['./questionnaire-stepper.component.scss']
})
export class QuestionnaireStepperComponent implements OnInit {
  @Input() questionnaire:Questionnaire;

  ngOnInit(): void {
  }

}
