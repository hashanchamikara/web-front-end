import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Questionnaire} from "@satipasala/base";
import {QuestionnaireService} from "@satipasala/base";
import {MatSnackBar} from "@angular/material";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'admin-questionnaire-drag-drop',
  templateUrl: './questionnaire-drag-drop.component.html',
  styleUrls: ['./questionnaire-drag-drop.component.scss']
})
export class QuestionnaireDragDropComponent implements OnInit {
  @Input() questionnaire:Questionnaire;
  @Input() connectedTo:string[]=[];
  @Output() onQuestionnaireUpdated:EventEmitter<Questionnaire> = new EventEmitter<Questionnaire>();
  @ViewChild('dragDropList',{static:true}) dragDropList: ElementRef;
  constructor() { }
  ngOnInit() {
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.onQuestionnaireUpdated.emit(this.questionnaire);
    }
  }


}
