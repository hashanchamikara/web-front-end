import {FirebaseDataSource, Questionnaire} from "@satipasala/base";
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionnaireService} from "@satipasala/base";

export class QuestionnaireDataSource extends FirebaseDataSource<Questionnaire>{

  constructor(protected paginator: MatPaginator, protected sort: MatSort,
              protected questionnaireService: QuestionnaireService) {
    super(paginator, sort, questionnaireService);
    this.setOrderBy({fieldPath:"name"})
  }
}
