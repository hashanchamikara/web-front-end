import {FirebaseDataSource, Question} from "@satipasala/base";
import {MatPaginator, MatSort} from "@angular/material";
import {QuestionsService} from "@satipasala/base";

export class QuestionsListDataSource extends FirebaseDataSource<Question<any>>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public questionnaireService: QuestionsService) {
    super(paginator, sort, questionnaireService);
    this.setOrderBy({fieldPath:"label"})
  }

}
