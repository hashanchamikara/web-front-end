import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {QuestionnaireService, QuestionsService, RefDataType, ReferenceDataService} from "@satipasala/base";
import {Label, QuestionType} from "@satipasala/base";
import {
  Question,
  QuestionLevel,
  ScoringMechanism
} from "../../../../../../../../libs/base/src/lib/model/Question";

import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'admin-question-creation-page',
  templateUrl: './question-creation-page.component.html',
  styleUrls: ['./question-creation-page.component.scss']
})
export class QuestionCreationPageComponent implements OnInit {
  static editMode: "edit" = "edit";
  static addMode: "add" = "add";
  mode: "edit" | "add";


  question: Question<any>;

  questionsTypes: QuestionType[];
  questionName: string;
  questionTypes: QuestionType[];
  questionLevels: QuestionLevel[];
  scoringMechanisms: ScoringMechanism[];
  questionLabels: Label[];


  questionId?: string;

  get questionLevelName(): string {
      return this.question.questionLevel.name;
  }

  set questionLevelName(name: string) {
    this.questionForm.patchValue({questionLevel: name});
    this.question.questionLevel = this.questionLevels.filter(level => level.name === name)[0]
  }

  get scoringMechanismType(): string {
    if (this.question.scoringMechanism != null) {
      return this.question.scoringMechanism.type;
    } else {
      return null;
    }

  }

  set scoringMechanismType(type: string) {

    this.questionForm.patchValue({scoringMechanism: type});
    this.question.scoringMechanism = this.scoringMechanisms.filter(mechanism => mechanism.type === type)[0]
  }

  get selectedQuestionType(): string {
    if (this.question.questionType != null) {
      return this.question.questionType.id;
    } else {
      return null;
    }

  }

  set selectedQuestionType(id: string) {
    this.questionForm.patchValue({questionType: id});
    this.question.questionType = this.questionsTypes.filter(value => value.id === id)[0];
    this.question.type = this.question.questionType.fieldType;
    this.question.options = this.referenceDataServie.getOptionsByQuetionType(this.question.questionType.answerType);
  }


  questionForm = this.fb.group({
    questionType: [null, Validators.required],
    label: [null, Validators.required],
    description: [null, Validators.required],
    questionLevel: [null, Validators.required],
    scoringMechanism: [null, Validators.required],
    labels: [null,Validators.required],
    /* firstName: [null, Validators.required],
     lastName: [null, Validators.required],
     address: [null, Validators.required],
     address2: null,
     city: [null, Validators.required],
     postalCode: [null, Validators.compose([
       Validators.required, Validators.minLength(5), Validators.maxLength(5)])
     ],*/
  });


  constructor(private fb: FormBuilder, private referenceDataServie: ReferenceDataService,
              private questionsService: QuestionsService, private _snackBar: MatSnackBar, private route: ActivatedRoute) {
    this.questionLabels = this.referenceDataServie.getQuestionLabels();
    this.referenceDataServie.getData(RefDataType.QUESTION_TYPE).subscribe(dataArr => {
      this.questionTypes = dataArr;
      this.questionsTypes = dataArr;//TODO: remove usage of two arrays
    });
    this.scoringMechanisms = this.referenceDataServie.getQuestionScoringMechanisms();
    this.questionLevels = this.referenceDataServie.getQuestionLevels();
    /*this.question.labels = [
      {
        category: "question_group",
        type: "chip",
        value: "Beginner"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Advanced"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Intermediate"
      }
    ];*/


  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.questionId) {
        this.questionId = params.questionId;
        if (this.questionId != null) {
          this.questionsService.get(this.questionId).subscribe(question => {
            this.mode = QuestionCreationPageComponent.editMode;
            this.fillForm(question);
          });
        }
      } else {
        this.mode = QuestionCreationPageComponent.addMode;
        this.fillForm(<Question<any>>{
          labels: [],
          questionLevel:{},
          scoringMechanism:{},
          questionType:{},
        });
      }
    });
  }

  onLabelChange(checked, label: Label) {
    if (checked == true) {
      this.addLabel(label);
    } else {
      this.removeLabel(label);
    }

    if (this.question.labels.length > 0) {
      this.questionForm.patchValue({labels: this.question.labels});
    } else {
      this.questionForm.patchValue({labels: null});
    }
  }


  addLabel(label: Label) {
    this.question.labels.push(label);
  }

  removeLabel(label: Label) {
    let index = this.question.labels.indexOf(label);
    this.question.labels.splice(index);
  }

  questionNotification(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  fillForm(question: Question<any>) {
    /* for (let field in question) {
       this.questionForm.controls[field].setValue(question[field], {emitEvent: true});
     }
 */
    this.question = question;
    this.questionForm.patchValue({questionLevel:  this.question.questionLevel.name});
    this.questionForm.patchValue({scoringMechanism: this.question.scoringMechanism.type});
    this.questionForm.patchValue({questionType: this.question.questionType.id});
    this.questionForm.patchValue({labels: this.question.labels});
  }

  onSubmit(values) {
    //this.question = Object.assign(this.question, values);
    switch (this.mode) {
      case QuestionCreationPageComponent.addMode:
        this.addQuestion();
        break;
      case QuestionCreationPageComponent.editMode:
        this.updateQuestion();
        break;
    }
  }

  updateQuestion() {
    this.questionsService.update(this.question.id, this.question).then(value => {
      this.questionNotification("Question Update", "Success");
    }).catch(reason => {
      console.error(reason);
      this.questionNotification("Question Update", "Failed!!!");
    });
  }

  addQuestion() {
    this.questionsService.add(this.question).then(value => {
      this.questionNotification("Question add", "Success");
    }).catch(reason => {
      console.error(reason);
      this.questionNotification("Question add", "Failed!!!");
    })
  }

  isLabelChecked(label: Label): boolean {
    if (this.question.labels) {
      return this.question.labels.filter(value => value.name === label.name).length > 0
    } else {
      return false;
    }
  }

  reset(){
    this.questionForm.reset();
  }


}
