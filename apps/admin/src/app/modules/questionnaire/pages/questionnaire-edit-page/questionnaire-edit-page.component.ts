import {Component, OnInit} from '@angular/core';
import {
  Questionnaire,
} from "@satipasala/base";
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionnaireService, QuestionsService, ReferenceDataService} from "@satipasala/base";
import {BaseQuestionnairePage} from "../base-questionnaire-page";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'admin-questionnaire-edit-page',
  templateUrl: './questionnaire-edit-page.component.html',
  styleUrls: ['./questionnaire-edit-page.component.scss']
})
export class QuestionnaireEditPage extends BaseQuestionnairePage {
  static editMode: "edit" = "edit";
  static addMode: "add" = "add";
  mode: "edit" | "add";

  questionnaireForm: FormGroup = this.fb.group({
    questionnaireName: [null, Validators.required],
    questions: [null, Validators.required]
  });


  constructor(protected router: Router, protected route: ActivatedRoute,
              protected questionnaireService: QuestionnaireService, private fb: FormBuilder, private referenceDataServie: ReferenceDataService,
              private _snackBar: MatSnackBar) {
    super(router, route, questionnaireService);


  }

  onQuestionnaireFetch(questionnaire: Questionnaire) {
    if (questionnaire == null) {
      this.mode = QuestionnaireEditPage.addMode;
      this.questionnaire = <Questionnaire>{
        name: null,
        questions: []
      }
    } else {
      this.mode = QuestionnaireEditPage.editMode;
    }
    this.updateFormValues();
  }

  updateFormValues() {
    this.questionnaireForm.patchValue({
      "questionnaireName": this.questionnaire.name,
      "questions": this.questionnaire.questions
    })
  }

  addQuestionnaire() {
    this.questionnaireService.add(this.questionnaire).then(value => {
      this.questionNotification("Adding Questionnaire", "Success");
    }).catch(reason => {
      this.questionNotification("Adding Questionnaire", "Failed");
    })
  }

  updateQuestionnaire() {
    this.questionnaireService.update(this.questionnaire.id, this.questionnaire).then(value => {
      this.questionNotification("Updating Questionnaire", "Success");
    }).catch(reason => {
      this.questionNotification("Updating Questionnaire", "Failed");
    })
  }

  saveQuestionnaire() {
    switch (this.mode) {
      case QuestionnaireEditPage.addMode:
        this.addQuestionnaire();
        break;
      case QuestionnaireEditPage.editMode:
        this.updateQuestionnaire();
        break;
    }
  }


  questionNotification(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  reset() {
    this.questionnaireForm.reset();
  }


}
