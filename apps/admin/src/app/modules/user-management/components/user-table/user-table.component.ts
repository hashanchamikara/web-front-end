import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {UsersService, RolesService} from "@satipasala/base";
import {FirebaseDataSource} from "@satipasala/base";
import {Role, User} from "@satipasala/base";
import {UserTableDatasource} from "./user-table.datasource";
import {Observable} from "rxjs";
import {Filter} from "../../../../../../../../libs/base/src/lib/impl/FirebaseDataSource";

@Component({
  selector: 'admin-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  dataSource: FirebaseDataSource<User>;


  displayedColumns = ['displayName', 'email', 'status', 'userRole', 'organizationType' ,'edit'];
  roles: Observable<Role[]>;
  status = ['Active', 'Inactive']; //todo add to reference data and retrieve from reference data service
  orgType = ['School', 'Temple']; //todo retrieve from reference data service

  pageEvent: PageEvent;

  constructor(private usersService: UsersService, private rolesService: RolesService) {
  }

  ngOnInit() {
    this.roles = this.rolesService.getAll();
    console.log("user roles size" + this.roles);
    this.dataSource = new UserTableDatasource(this.paginator, this.sort, this.usersService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  applyFilter(filterValue: string) {
    this.dataSource["filter"] = filterValue.trim().toLowerCase();
  }



  resetFilters() {
    //this.dataSource = new StudentDataSource(this.studentDatabase, this.sort);
    this.dataSource.clearFilters();
  }

  filterByStatus (value){
    let filter:Filter = new Filter()
    filter.fieldPath = "status";
    filter.value = value;
    filter.opStr = "==";

    this.dataSource.addFilter(filter)
  }

  filterByOrg (value){
    let filter:Filter = new Filter()
    filter.fieldPath = "organization";
    filter.value = value;
    filter.opStr = "==";
    this.dataSource.addFilter(filter)
  }

  filterByRole (value){
    let filter:Filter = new Filter()
   filter.fieldPath = "userRole";
    if (value == "System Administrator")
    {
      value = "SYSTEMADMIN";
    }
    else if (value == "Read only access to the systerm")
    {
      value = "READONLY";
    }
    filter.value = value;
    filter.opStr = "==";
    this.dataSource.addFilter(filter)
  }
}

