import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {User} from "@satipasala/base";
import {AfterViewInit} from "@angular/core";
import {FirebaseDataSource} from "@satipasala/base";
import {UsersService} from "@satipasala/base";

/**
 * Data source for the UserManagementPage view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class UserTableDatasource extends FirebaseDataSource<User> implements AfterViewInit {
  // userId: string;
  // public static subCollection:string = "roles";

  constructor(private paginator: MatPaginator, private sort: MatSort,
              public usersService: UsersService) {
    super(paginator, sort, usersService);
    this.setOrderBy({fieldPath:"uid"});
  }
}
