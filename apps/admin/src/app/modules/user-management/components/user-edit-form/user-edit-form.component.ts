import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import * as APP_ROUTES from "../../../../app-routs";
import {Role, User, UsersService} from "@satipasala/base";
import {Observable} from "rxjs";

type UserFields = 'firstName' | 'dob' | 'phoneNumber' | 'email' | 'password' | 'city' | 'organization' | 'userRole' | 'preferredMedium';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'admin-user-edit-form',
  templateUrl: './user-edit-form.component.html',
  styleUrls: ['./user-edit-form.component.scss']
})
export class UserEditFormComponent implements OnInit {

  userEditForm: FormGroup;

  public user: User;
  public uid: string;
  public formTitle: string;

  formErrors: FormErrors = {
    'firstName': '',
    'dob': '',
    'phoneNumber': '',
    'email': '',
    'password': '',
    'city': '',
    'organization': '',
    'userRole': '',
    'preferredMedium': ''
  };
  validationMessages = {
    'firstName': {
      'required': 'First Name is required.',
      'maxlength': 'First Name cannot be more than 40 characters long.'
    },
    'dob': {
      'required': 'Date of Birth is required.',
    },
    'phoneNumber': {
      'required': 'Phone Number is required.',
    },
    'email': {
      'required': 'User Role is required.',
      'email': 'Email must be a valid email.'
    },
    'password': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'confirmPassword': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
    'city': {
      'required': 'City is required.'
    },
    'organization': {
      'required': 'Organization is required.'
    },
    'role': {
      'required': 'Role is required.'
    },
    'preferredMedium': {
      'required': 'Preferred Medium is required.'
    }
  };

  userRoles: Observable<Role[]>;

  preferredMediums: string[] = [
    "English",
    "Sinhala",
    "Tamil"
  ];

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private UsersService: UsersService) {
  }

  ngOnInit(): void {
    this.buildForm();
    this.userEditForm = this.formBuilder.group({
      uid: '',
      firstName: '',
      lastName: '',
      displayName: '',
      email: '',
      dob: '',
      nic: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '',
      city: '',
      organization: '',
      userRole: '',
      preferredMedium: '',
      street: '',
      country: '',
      province: ''
    });
    this.route.params.subscribe(params => {
      this.uid = params.uid;
      this.UsersService.getUser(params.uid, this.setUser(this.userEditForm));
    });
  }

  setUser(form) {
    return user => {
      console.log(user);
      if (user) {
        form.setValue({
          uid: user.uid,
          firstName: user.firstName,
          lastName: user.lastName,
          displayName: user.displayName,
          email: user.email,
          dob: user.dob,
          nic: user.nic,
          phoneNumber: user.phoneNumber,
          password: user.password,
          confirmPassword: user.password,
          city: user.city,
          organization: user.organization,
          userRole: user.userRole,
          preferredMedium: user.preferredMedium,
          street: user.street,
          country: user.country,
          province: user.province
        });
      } else {
        this.backToUserManage();
      }
    };
  }

  editUser() {
    this.user = this.userEditForm.value;
    this.UsersService.update(this.uid, this.user);
  }

  backToUserManage() {
    console.log(APP_ROUTES.USERS_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.USERS_ROUTE);
  }

  buildForm() {
    this.userEditForm = this.formBuilder.group({
      'firstName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'dob': ['', [
        Validators.required
      ]],
      'phoneNumber': ['', [
        Validators.pattern('[0-9]{10}'),
        Validators.required
      ]],
      'userRole': ['', [
        Validators.required,
      ]],
      'email': ['', [
        Validators.required,
        Validators.email,
      ]],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'confirmPassword': ['', [
        Validators.required,
      ]],
      'city': ['', [
        Validators.required,
      ]],
      'organization': ['', [
        Validators.required,
      ]],
      'preferredMedium': ['', [
        Validators.required,
      ]],

      'lastName': [],
      'nic': [],
      'street': [],
      'province': [],
      'country': [],
    });

    this.userEditForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    if (!this.userEditForm) {
      return;
    }
    const form = this.userEditForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field) && (field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
                this.formErrors[field] += `${(messages as { [key: string]: string })[key]} `;
              }
            }
          }
        }
      }
    }
  }
}
