import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellRoutingModule } from './app-shell-routing.module';
import {MaterialModule} from "../../imports/material.module";
import {AppShellComponent} from "./app-shell.component/app-shell.component";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {AngularFireFunctionsModule} from "@angular/fire/functions";
import {BrowserTransferStateModule} from "@angular/platform-browser";
import 'hammerjs';
import {AuthGuard, AuthService, BaseModule} from "@satipasala/base";
import {ErrorStateMatcher, MAT_LABEL_GLOBAL_OPTIONS, ShowOnDirtyErrorStateMatcher} from "@angular/material";
import {CoreClientModule} from "../core/core-client.module";

@NgModule({
  declarations: [AppShellComponent,],
  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    CommonModule,
    CoreClientModule,
    BrowserTransferStateModule,
    AppShellRoutingModule,
    BaseModule
  ],
  providers: [AuthService,AuthGuard,
    {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}],
  bootstrap:[AppShellComponent]

})
export class AppShellModule { }
