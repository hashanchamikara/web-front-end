import {Component, Input, NgModule, OnInit} from '@angular/core';



@Component({
  selector: 'client-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent implements OnInit {
  isMobile:boolean = false
  constructor() { }
  ngOnInit() {

  }
  layouts = [
    {'id': 1, 'name': 'Angular'},
    {'id': 2, 'name': 'Router'},
    {'id': 3, 'name': 'Mongo DB'},
    {'id': 4, 'name': 'Http request'},
  ];

  // isMobile(isMobile){
  //   this.isMobile = isMobile;
  // }

}
