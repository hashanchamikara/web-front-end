import { CoreClientModule } from './core.module';

describe('CoreModule', () => {
  let coreModule: CoreClientModule;

  beforeEach(() => {
    coreModule = new CoreClientModule();
  });

  it('should create an instance', () => {
    expect(coreModule).toBeTruthy();
  });
});
