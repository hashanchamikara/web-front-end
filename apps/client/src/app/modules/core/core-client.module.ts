import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule, MatIconModule,
  MatInputModule, MatMenuModule, MatPaginatorModule,
  MatRadioModule,
  MatSelectModule, MatSortModule,
  MatTableModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../../imports/material.module";
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { MenuComponent } from './components/menu/menu.component';
import { NotificationComponent } from './components/notification/notification.component';
import {AngularFirestore} from "@angular/fire/firestore";
import {CoreModule} from "@satipasala/core";
import {AuthService, BaseModule} from "@satipasala/base";

@NgModule({
  providers: [AngularFirestore,AuthService],

  declarations: [
  UserProfileComponent,
  MenuComponent,
  NotificationComponent,
 ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MaterialModule,
    CoreModule,
    BaseModule
  ],

  exports: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent
  ]
})
export class CoreClientModule {
}
