import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellRoutingModule } from './app-shell-routing.module';
import { MaterialModule } from "../../imports/material.module";
import { AppShellComponent } from "./app-shell.component/app-shell.component";
import { AuthGuard, BaseModule } from "@satipasala/base";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireFunctionsModule } from "@angular/fire/functions";
import { BrowserTransferStateModule } from "@angular/platform-browser";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { ErrorStateMatcher, MAT_LABEL_GLOBAL_OPTIONS, ShowOnDirtyErrorStateMatcher } from "@angular/material";
import 'core-js/es/reflect';
import 'zone.js';
import { AuthService } from "@satipasala/base";
import { CoreWebModule } from "../core/core-web.module";
import { CoreModule } from "@satipasala/core";



@NgModule({
  declarations: [AppShellComponent,],
  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    AngularFontAwesomeModule,
    CommonModule,
    CoreWebModule,
    BrowserTransferStateModule,
    AppShellRoutingModule,
    CoreModule,
    BaseModule
  ],
  providers: [AuthService, AuthGuard,
    { provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: { float: 'always' } },
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }],
  bootstrap: [AppShellComponent]

})
export class AppShellModule { }
