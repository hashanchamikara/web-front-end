import {Component, OnInit, ViewChild} from '@angular/core';





@Component({
  selector: 'web-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent implements OnInit {

  // isMobile:boolean = false
  constructor() { }
  ngOnInit() {

  }


  departmentDetails = [
    {'id': 1, name: 'Angular', sub: 'sama'},
    {'id': 2, name: 'Router', sub: 'sama'},
    {'id': 3, name: 'Mongo DB', sub: 'sama'},
    {'id': 4, name: 'Http request', sub: 'sama'},
  ];

}
