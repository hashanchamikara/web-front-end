import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from "../../imports/material.module";
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { MenuComponent } from './components/menu/menu.component';
import { NotificationComponent } from './components/notification/notification.component';
import { AngularFirestore } from "@angular/fire/firestore";
import { AuthService, BaseModule } from "@satipasala/base";
import { SearchComponent } from "../search/components/search.component";
import { EventBannerComponent } from "../event/components/event-banner/event-banner.component";
import { CoreModule } from "@satipasala/core";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeAboutComponent } from './components/home-about/home-about.component';


@NgModule({
  providers: [AngularFirestore, AuthService],

  declarations: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent,
    SearchComponent,
    EventBannerComponent,
    HomeAboutComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    MaterialModule,
    CoreModule,
    BaseModule
  ],

  exports: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent,
    SearchComponent,
    EventBannerComponent,
    HomeAboutComponent

  ]
})
export class CoreWebModule {
}
