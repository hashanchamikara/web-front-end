import { CoreWebModule } from './core.module';

describe('CoreModule', () => {
  let coreModule: CoreWebModule;

  beforeEach(() => {
    coreModule = new CoreWebModule();
  });

  it('should create an instance', () => {
    expect(coreModule).toBeTruthy();
  });
});
