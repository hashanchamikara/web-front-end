import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
// import {ResizedEvent} from 'angular-resize-event';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'web-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None, // disable ViewEncapsulation
})
export class MenuComponent implements OnInit {
  selectedMenuItem = 'Home';

  constructor(@Inject(DOCUMENT) private document: Document) { }

  ngOnInit() {
  }

  mainNav = [
    { headingName: 'Home' },
    { headingName: 'Resources' },
    { headingName: 'Registration' },
    { headingName: 'Events' },
    { headingName: 'Support Us' },
    { headingName: 'About Us' },
    { headingName: 'contact Us' },
  ];


  onMenuItemClicked(menuItem: mainNav) {
    console.log('clicked ' + menuItem);
    this.selectedMenuItem = menuItem.headingName;
  }
}


export class mainNav {
  headingName: string;
  icon: string;
  dropDown: boolean;
  // subCategory: subNavItem[];
  headingLink: string;
  isNavbarCollapsed: boolean = true;

  constructor(_categoryName: string, _icon: string, _dropDown: boolean, categoryLink: string = "") {
    this.headingName = _categoryName;
    this.icon = _icon;
    this.dropDown = _dropDown;
    // this.subCategory = _subCategory;
    this.headingLink = categoryLink;

  }
}
