import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SthLibraryRoutingModule } from './sth library-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SthLibraryRoutingModule
  ]
})
export class SthLibraryModule { }
